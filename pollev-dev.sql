-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: mysql380.umbler.com
-- Generation Time: 12-Dez-2018 às 23:57
-- Versão do servidor: 5.6.40-log
-- PHP Version: 5.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pollev-dev`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-08-15 00:46:05', '2018-08-15 03:46:05', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 67, 'WooCommerce', 'woocommerce@pollev-com.umbler.net', '', '', '2018-08-22 10:04:38', '2018-08-22 13:04:38', 'Pedido cancelado por falta de pagamento - tempo limite ultrapassado. Status do pedido alterado de Pagamento pendente para Cancelado.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(3, 70, 'WooCommerce', 'woocommerce@pollev-com.umbler.net', '', '', '2018-08-23 00:49:16', '2018-08-23 03:49:16', 'Pedido cancelado por falta de pagamento - tempo limite ultrapassado. Status do pedido alterado de Pagamento pendente para Cancelado.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 82, 'WooCommerce', 'woocommerce@pollev-com.umbler.net', '', '', '2018-08-23 18:05:30', '2018-08-23 21:05:30', 'PagSeguro: O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento. Status do pedido alterado de Pagamento pendente para Aguardando.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(5, 84, 'WooCommerce', 'woocommerce@pollev-com.umbler.net', '', '', '2018-08-25 12:08:58', '2018-08-25 15:08:58', 'PagSeguro: O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento. Status do pedido alterado de Pagamento pendente para Aguardando.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(6, 84, 'pollev', 'asjesus1994@gmail.com', '', '', '2018-08-25 12:29:28', '2018-08-25 15:29:28', 'Status do pedido alterado de Aguardando para Concluído.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(7, 109, 'WooCommerce', 'woocommerce@pollev-com.umbler.net', '', '', '2018-10-08 20:46:03', '2018-10-08 23:46:03', 'PagSeguro: O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento. Status do pedido alterado de Pagamento pendente para Aguardando.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(8, 112, 'WooCommerce', 'woocommerce@pollev-com.umbler.net', '', '', '2018-10-17 01:20:37', '2018-10-17 04:20:37', 'PagSeguro: O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento. Status do pedido alterado de Pagamento pendente para Aguardando.', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM AUTO_INCREMENT=2447 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://pollev-com.umbler.net/', 'yes'),
(2, 'home', 'http://pollev-com.umbler.net/', 'yes'),
(3, 'blogname', 'Pollev Brasil', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'asjesus1994@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:158:{s:24:"^wc-auth/v([1]{1})/(.*)?";s:63:"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]";s:22:"^wc-api/v([1-3]{1})/?$";s:51:"index.php?wc-api-version=$matches[1]&wc-api-route=/";s:24:"^wc-api/v([1-3]{1})(.*)?";s:61:"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]";s:7:"loja/?$";s:27:"index.php?post_type=product";s:37:"loja/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"loja/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"loja/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:56:"categoria-produto/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:51:"categoria-produto/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:32:"categoria-produto/(.+?)/embed/?$";s:44:"index.php?product_cat=$matches[1]&embed=true";s:44:"categoria-produto/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:26:"categoria-produto/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:52:"produto-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:47:"produto-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:28:"produto-tag/([^/]+)/embed/?$";s:44:"index.php?product_tag=$matches[1]&embed=true";s:40:"produto-tag/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:22:"produto-tag/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:35:"produto/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"produto/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"produto/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produto/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"produto/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:41:"produto/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:24:"produto/([^/]+)/embed/?$";s:40:"index.php?product=$matches[1]&embed=true";s:28:"produto/([^/]+)/trackback/?$";s:34:"index.php?product=$matches[1]&tb=1";s:48:"produto/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:43:"produto/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?product=$matches[1]&feed=$matches[2]";s:36:"produto/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&paged=$matches[2]";s:43:"produto/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?product=$matches[1]&cpage=$matches[2]";s:33:"produto/([^/]+)/wc-api(/(.*))?/?$";s:48:"index.php?product=$matches[1]&wc-api=$matches[3]";s:39:"produto/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"produto/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:32:"produto/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?product=$matches[1]&page=$matches[2]";s:24:"produto/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:34:"produto/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:54:"produto/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produto/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:49:"produto/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"produto/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=16&cpage=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:62:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$";s:99:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]";s:62:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:73:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:25:"(.?.+?)/orders(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&orders=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:28:"(.?.+?)/downloads(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&downloads=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:34:"(.?.+?)/payment-methods(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&payment-methods=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:40:"(.?.+?)/delete-payment-method(/(.*))?/?$";s:64:"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]";s:45:"(.?.+?)/set-default-payment-method(/(.*))?/?$";s:69:"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:45:"woocommerce-correios/woocommerce-correios.php";i:1;s:93:"woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php";i:2;s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";i:3;s:27:"woocommerce/woocommerce.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:4:{i:0;s:73:"C:\\wamp64\\www\\WordPress/wp-content/themes/storefront/content-homepage.php";i:1;s:64:"C:\\wamp64\\www\\WordPress/wp-content/themes/storefront/archive.php";i:2;s:62:"C:\\wamp64\\www\\WordPress/wp-content/themes/storefront/style.css";i:3;s:0:"";}', 'no'),
(40, 'template', 'storefront', 'yes'),
(41, 'stylesheet', 'storefront', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(84, 'page_on_front', '16', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:114:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:1:{s:4:"read";b:1;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop manager";s:12:"capabilities";a:92:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:8:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:0:{}s:8:"header-1";a:0:{}s:8:"footer-1";a:0:{}s:8:"footer-2";a:0:{}s:8:"footer-3";a:0:{}s:8:"footer-4";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(112, 'cron', 'a:13:{i:1544669166;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1544669649;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1544669650;a:1:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1544670000;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544672766;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1544672783;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544672989;a:1:{s:33:"woocommerce_cleanup_personal_data";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544672999;a:1:{s:30:"woocommerce_tracker_send_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544673189;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544683789;a:1:{s:24:"woocommerce_cleanup_logs";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1544694589;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1546560000;a:1:{s:25:"woocommerce_geoip_updater";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:7:"monthly";s:4:"args";a:0:{}s:8:"interval";i:2635200;}}}s:7:"version";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1534306449;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(150, 'woocommerce_store_address', 'Rua Gilberto Gil', 'yes'),
(151, 'woocommerce_store_address_2', 'Rua Gilberto Gil', 'yes'),
(152, 'woocommerce_store_city', 'Salvador', 'yes'),
(153, 'woocommerce_default_country', 'BR:BA', 'yes'),
(154, 'woocommerce_store_postcode', '41.260-190', 'yes'),
(155, 'woocommerce_allowed_countries', 'specific', 'yes'),
(156, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(157, 'woocommerce_specific_allowed_countries', 'a:1:{i:0;s:2:"BR";}', 'yes'),
(158, 'woocommerce_ship_to_countries', 'specific', 'yes'),
(159, 'woocommerce_specific_ship_to_countries', 'a:1:{i:0;s:2:"BR";}', 'yes'),
(160, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(161, 'woocommerce_calc_taxes', 'yes', 'yes'),
(162, 'woocommerce_enable_coupons', 'yes', 'yes'),
(163, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(164, 'woocommerce_currency', 'BRL', 'yes'),
(2443, '_site_transient_timeout_theme_roots', '1544667851', 'no'),
(2444, '_site_transient_theme_roots', 'a:1:{s:10:"storefront";s:7:"/themes";}', 'no'),
(2438, '_transient_wc_product_loop7f651543071184', 'O:8:"stdClass":5:{s:3:"ids";a:2:{i:0;i:9;i:1;i:49;}s:5:"total";i:2;s:11:"total_pages";i:1;s:8:"per_page";i:2;s:12:"current_page";i:1;}', 'no'),
(2440, '_transient_wc_product_loopa47f1543071184', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:49;i:1;i:47;i:2;i:85;i:3;i:9;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(128, 'can_compress_scripts', '1', 'no'),
(2133, '_transient_wc_attribute_taxonomies', 'a:2:{i:0;O:8:"stdClass":6:{s:12:"attribute_id";s:1:"1";s:14:"attribute_name";s:8:"material";s:15:"attribute_label";s:8:"Material";s:14:"attribute_type";s:6:"select";s:17:"attribute_orderby";s:10:"menu_order";s:16:"attribute_public";s:1:"0";}i:1;O:8:"stdClass":6:{s:12:"attribute_id";s:1:"2";s:14:"attribute_name";s:4:"peso";s:15:"attribute_label";s:4:"Peso";s:14:"attribute_type";s:6:"select";s:17:"attribute_orderby";s:4:"name";s:16:"attribute_public";s:1:"0";}}', 'yes'),
(2146, '_transient_timeout_wc_shipping_method_count_0_1534995675', '1545660328', 'no'),
(2147, '_transient_wc_shipping_method_count_0_1534995675', '3', 'no'),
(2158, '_transient_timeout_wc_product_loop7f651543063296', '1545660914', 'no'),
(2159, '_transient_wc_product_loop7f651543063296', 'O:8:"stdClass":5:{s:3:"ids";a:2:{i:0;i:9;i:1;i:49;}s:5:"total";i:2;s:11:"total_pages";i:1;s:8:"per_page";i:2;s:12:"current_page";i:1;}', 'no'),
(2160, '_transient_timeout_wc_product_loopa47f1543063296', '1545660915', 'no'),
(2161, '_transient_wc_product_loopa47f1543063296', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:49;i:1;i:47;i:2;i:85;i:3;i:9;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2304, '_transient_timeout_wc_term_counts', '1547222078', 'no'),
(2305, '_transient_wc_term_counts', 'a:8:{i:20;s:1:"1";i:16;s:1:"2";i:19;s:1:"3";i:18;s:1:"1";i:21;s:0:"";i:23;s:0:"";i:22;s:0:"";i:15;s:1:"0";}', 'no'),
(2439, '_transient_timeout_wc_product_loopa47f1543071184', '1547222078', 'no'),
(2309, '_transient_timeout_wc_product_loop308c1543071184', '1545937966', 'no'),
(2310, '_transient_wc_product_loop308c1543071184', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:127;i:1;i:117;i:2;i:85;i:3;i:49;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2311, '_transient_timeout_wc_product_loopa5b41543071184', '1545937967', 'no'),
(2312, '_transient_wc_product_loopa5b41543071184', 'O:8:"stdClass":5:{s:3:"ids";a:0:{}s:5:"total";i:0;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2313, '_transient_timeout_wc_product_loop1a121543071184', '1545937967', 'no'),
(2302, '_transient_wc_count_comments', 'O:8:"stdClass":7:{s:14:"total_comments";i:1;s:3:"all";i:1;s:8:"approved";s:1:"1";s:9:"moderated";i:0;s:4:"spam";i:0;s:5:"trash";i:0;s:12:"post-trashed";i:0;}', 'yes'),
(2303, '_transient_is_multi_author', '0', 'yes'),
(2314, '_transient_wc_product_loop1a121543071184', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:9;i:1;i:40;i:2;i:47;i:3;i:49;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2316, '_transient_timeout_wc_products_onsale', '1545937967', 'no'),
(2317, '_transient_wc_products_onsale', 'a:3:{i:0;i:9;i:1;i:47;i:2;i:49;}', 'no'),
(2318, '_transient_timeout_wc_product_loope5e11543071184', '1545937967', 'no'),
(2319, '_transient_wc_product_loope5e11543071184', 'O:8:"stdClass":5:{s:3:"ids";a:3:{i:0;i:49;i:1;i:47;i:2;i:9;}s:5:"total";i:3;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2320, '_transient_timeout_wc_product_loop73ac1543071184', '1545937967', 'no'),
(2321, '_transient_wc_product_loop73ac1543071184', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:49;i:1;i:85;i:2;i:47;i:3;i:127;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(1081, 'woocommerce_pagseguro_settings', 'a:20:{s:7:"enabled";s:3:"yes";s:5:"title";s:9:"PagSeguro";s:11:"description";s:19:"Pagar com PagSeguro";s:11:"integration";s:0:"";s:6:"method";s:11:"transparent";s:7:"sandbox";s:2:"no";s:5:"email";s:21:"asjesus1994@gmail.com";s:5:"token";s:32:"5F4275322CC24371AC4F1779C4ADF7B2";s:13:"sandbox_email";s:0:"";s:13:"sandbox_token";s:0:"";s:20:"transparent_checkout";s:0:"";s:9:"tc_credit";s:3:"yes";s:11:"tc_transfer";s:2:"no";s:9:"tc_ticket";s:3:"yes";s:17:"tc_ticket_message";s:3:"yes";s:8:"behavior";s:0:"";s:15:"send_only_total";s:2:"no";s:14:"invoice_prefix";s:7:"Pollev-";s:7:"testing";s:0:"";s:5:"debug";s:3:"yes";}', 'yes'),
(827, 'product_cat_children', 'a:1:{i:16;a:2:{i:0;i:22;i:1;i:23;}}', 'yes'),
(982, '_transient_orders-transient-version', '1539750034', 'yes'),
(2103, '_transient_timeout_wc_shipping_method_count_1_1534995675', '1545653496', 'no'),
(2104, '_transient_wc_shipping_method_count_1_1534995675', '3', 'no'),
(2120, '_transient_timeout_wc_product_loop308c1543063296', '1545655874', 'no'),
(2096, '_transient_timeout_wc_product_loop7f651536068923', '1545653135', 'no'),
(2097, '_transient_wc_product_loop7f651536068923', 'O:8:"stdClass":5:{s:3:"ids";a:2:{i:0;i:9;i:1;i:49;}s:5:"total";i:2;s:11:"total_pages";i:1;s:8:"per_page";i:2;s:12:"current_page";i:1;}', 'no'),
(2121, '_transient_wc_product_loop308c1543063296', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:85;i:1;i:49;i:2;i:47;i:3;i:40;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2122, '_transient_timeout_wc_product_loopa5b41543063296', '1545655875', 'no'),
(2123, '_transient_wc_product_loopa5b41543063296', 'O:8:"stdClass":5:{s:3:"ids";a:0:{}s:5:"total";i:0;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2124, '_transient_timeout_wc_product_loop1a121543063296', '1545655875', 'no'),
(2125, '_transient_wc_product_loop1a121543063296', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:9;i:1;i:40;i:2;i:47;i:3;i:49;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2126, '_transient_timeout_wc_product_loope5e11543063296', '1545655875', 'no'),
(2127, '_transient_wc_product_loope5e11543063296', 'O:8:"stdClass":5:{s:3:"ids";a:3:{i:0;i:49;i:1;i:47;i:2;i:9;}s:5:"total";i:3;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2128, '_transient_timeout_wc_product_loop73ac1543063296', '1545655875', 'no'),
(2129, '_transient_wc_product_loop73ac1543063296', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:49;i:1;i:85;i:2;i:47;i:3;i:40;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2435, '_transient_timeout_wc_featured_products', '1547222078', 'no'),
(2436, '_transient_wc_featured_products', 'a:0:{}', 'no'),
(2437, '_transient_timeout_wc_product_loop7f651543071184', '1547222078', 'no'),
(2098, '_transient_timeout_wc_product_loopa47f1536068923', '1545653135', 'no'),
(2099, '_transient_wc_product_loopa47f1536068923', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:49;i:1;i:47;i:2;i:85;i:3;i:9;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2066, '_transient_timeout_wc_product_loop308c1536068923', '1545527489', 'no'),
(2067, '_transient_wc_product_loop308c1536068923', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:85;i:1;i:49;i:2;i:47;i:3;i:40;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2068, '_transient_timeout_wc_product_loopa5b41536068923', '1545527490', 'no'),
(2069, '_transient_wc_product_loopa5b41536068923', 'O:8:"stdClass":5:{s:3:"ids";a:0:{}s:5:"total";i:0;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2070, '_transient_timeout_wc_product_loop1a121536068923', '1545527490', 'no'),
(2071, '_transient_wc_product_loop1a121536068923', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:9;i:1;i:40;i:2;i:47;i:3;i:49;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2445, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1544666052;s:7:"checked";a:1:{s:10:"storefront";s:5:"2.3.3";}s:8:"response";a:1:{s:10:"storefront";a:4:{s:5:"theme";s:10:"storefront";s:11:"new_version";s:5:"2.4.2";s:3:"url";s:40:"https://wordpress.org/themes/storefront/";s:7:"package";s:58:"https://downloads.wordpress.org/theme/storefront.2.4.2.zip";}}s:12:"translations";a:0:{}}', 'no'),
(2446, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1544666052;s:8:"response";a:2:{s:19:"akismet/akismet.php";O:8:"stdClass":12:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:3:"4.1";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:54:"https://downloads.wordpress.org/plugin/akismet.4.1.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:3:"5.0";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":12:{s:2:"id";s:25:"w.org/plugins/woocommerce";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:5:"3.5.2";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:60:"https://downloads.wordpress.org/plugin/woocommerce.3.5.2.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831";s:2:"1x";s:64:"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831";}s:7:"banners";a:2:{s:2:"2x";s:67:"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184";s:2:"1x";s:66:"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:3:"5.0";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:3:{s:45:"woocommerce-correios/woocommerce-correios.php";O:8:"stdClass":9:{s:2:"id";s:34:"w.org/plugins/woocommerce-correios";s:4:"slug";s:20:"woocommerce-correios";s:6:"plugin";s:45:"woocommerce-correios/woocommerce-correios.php";s:11:"new_version";s:5:"3.7.1";s:3:"url";s:51:"https://wordpress.org/plugins/woocommerce-correios/";s:7:"package";s:69:"https://downloads.wordpress.org/plugin/woocommerce-correios.3.7.1.zip";s:5:"icons";a:2:{s:2:"2x";s:73:"https://ps.w.org/woocommerce-correios/assets/icon-256x256.png?rev=1356952";s:2:"1x";s:73:"https://ps.w.org/woocommerce-correios/assets/icon-128x128.png?rev=1356952";}s:7:"banners";a:2:{s:2:"2x";s:76:"https://ps.w.org/woocommerce-correios/assets/banner-1544x500.png?rev=1356952";s:2:"1x";s:75:"https://ps.w.org/woocommerce-correios/assets/banner-772x250.png?rev=1356952";}s:11:"banners_rtl";a:0:{}}s:93:"woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php";O:8:"stdClass":9:{s:2:"id";s:58:"w.org/plugins/woocommerce-extra-checkout-fields-for-brazil";s:4:"slug";s:44:"woocommerce-extra-checkout-fields-for-brazil";s:6:"plugin";s:93:"woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php";s:11:"new_version";s:5:"3.6.1";s:3:"url";s:75:"https://wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil/";s:7:"package";s:93:"https://downloads.wordpress.org/plugin/woocommerce-extra-checkout-fields-for-brazil.3.6.1.zip";s:5:"icons";a:2:{s:2:"2x";s:97:"https://ps.w.org/woocommerce-extra-checkout-fields-for-brazil/assets/icon-256x256.png?rev=1356956";s:2:"1x";s:97:"https://ps.w.org/woocommerce-extra-checkout-fields-for-brazil/assets/icon-128x128.png?rev=1356956";}s:7:"banners";a:2:{s:2:"2x";s:100:"https://ps.w.org/woocommerce-extra-checkout-fields-for-brazil/assets/banner-1544x500.png?rev=1356956";s:2:"1x";s:99:"https://ps.w.org/woocommerce-extra-checkout-fields-for-brazil/assets/banner-772x250.png?rev=1356956";}s:11:"banners_rtl";a:0:{}}s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";O:8:"stdClass":9:{s:2:"id";s:35:"w.org/plugins/woocommerce-pagseguro";s:4:"slug";s:21:"woocommerce-pagseguro";s:6:"plugin";s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";s:11:"new_version";s:6:"2.13.1";s:3:"url";s:52:"https://wordpress.org/plugins/woocommerce-pagseguro/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/woocommerce-pagseguro.2.13.1.zip";s:5:"icons";a:2:{s:2:"2x";s:74:"https://ps.w.org/woocommerce-pagseguro/assets/icon-256x256.png?rev=1356960";s:2:"1x";s:74:"https://ps.w.org/woocommerce-pagseguro/assets/icon-128x128.png?rev=1356960";}s:7:"banners";a:2:{s:2:"2x";s:77:"https://ps.w.org/woocommerce-pagseguro/assets/banner-1544x500.png?rev=1356960";s:2:"1x";s:76:"https://ps.w.org/woocommerce-pagseguro/assets/banner-772x250.png?rev=1356960";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(141, 'recently_activated', 'a:0:{}', 'yes'),
(308, 'woocommerce_setup_jetpack_opted_in', '1', 'yes'),
(297, '_transient_shipping-transient-version', '1534995675', 'yes'),
(298, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:"title";s:9:"Taxa fixa";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:2:"10";}', 'yes'),
(299, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:"title";s:9:"Taxa fixa";s:10:"tax_status";s:7:"taxable";s:4:"cost";s:2:"15";}', 'yes'),
(300, 'mailchimp_woocommerce_plugin_do_activation_redirect', '', 'yes'),
(302, 'woocommerce_admin_notice_storefront_install_error', 'storefront não pode ser instalado (Unable to connect to the filesystem. Please confirm your credentials.). <a href="http://localhost/wordpress/wp-admin/update.php?action=install-theme&#038;theme=storefront&#038;_wpnonce=a9c18fafb8">Instale manualmente clicando aqui.</a>', 'yes'),
(303, 'woocommerce_admin_notice_mailchimp-for-woocommerce_install_error', 'MailChimp para WooCommerce não pode ser instalado (Ocorreu um erro inesperado. Algo pode estar errado com o WordPress.org ou com a configuração deste servidor. Se os problemas persistirem, busque ajuda no <a href="https://br.wordpress.org/support/">fórum de suporte</a>.). <a href="http://localhost/wordpress/wp-admin/index.php?wc-install-plugin-redirect=mailchimp-for-woocommerce">Instale manualmente clicando aqui.</a>', 'yes'),
(165, 'woocommerce_currency_pos', 'left', 'yes'),
(166, 'woocommerce_price_thousand_sep', '.', 'yes'),
(167, 'woocommerce_price_decimal_sep', ',', 'yes'),
(168, 'woocommerce_price_num_decimals', '2', 'yes'),
(169, 'woocommerce_shop_page_id', '5', 'yes'),
(170, 'woocommerce_cart_redirect_after_add', 'yes', 'yes'),
(171, 'woocommerce_enable_ajax_add_to_cart', 'no', 'yes'),
(172, 'woocommerce_weight_unit', 'kg', 'yes'),
(173, 'woocommerce_dimension_unit', 'cm', 'yes'),
(174, 'woocommerce_enable_reviews', 'yes', 'yes'),
(175, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(176, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(177, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(178, 'woocommerce_review_rating_required', 'yes', 'no'),
(179, 'woocommerce_manage_stock', 'yes', 'yes'),
(180, 'woocommerce_hold_stock_minutes', '60', 'no'),
(181, 'woocommerce_notify_low_stock', 'yes', 'no'),
(182, 'woocommerce_notify_no_stock', 'yes', 'no'),
(183, 'woocommerce_stock_email_recipient', 'asjesus1994@gmail.com', 'no'),
(184, 'woocommerce_notify_low_stock_amount', '5', 'no'),
(185, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(186, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(187, 'woocommerce_stock_format', 'low_amount', 'yes'),
(188, 'woocommerce_file_download_method', 'force', 'no'),
(189, 'woocommerce_downloads_require_login', 'no', 'no'),
(190, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(191, 'woocommerce_prices_include_tax', 'no', 'yes'),
(192, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(193, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(194, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(195, 'woocommerce_tax_classes', 'Taxa reduzida\r\nTaxa zero', 'yes'),
(196, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(197, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(198, 'woocommerce_price_display_suffix', '', 'yes'),
(199, 'woocommerce_tax_total_display', 'itemized', 'no'),
(200, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(201, 'woocommerce_shipping_cost_requires_address', 'yes', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(202, 'woocommerce_ship_to_destination', 'shipping', 'no'),
(203, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(204, 'woocommerce_enable_guest_checkout', 'no', 'no'),
(205, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(206, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(207, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(208, 'woocommerce_registration_generate_username', 'no', 'no'),
(209, 'woocommerce_registration_generate_password', 'no', 'no'),
(210, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(211, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(212, 'woocommerce_registration_privacy_policy_text', 'Seus dados pessoais serão usados para aprimorar a sua experiência em todo este site, para gerenciar o acesso a sua conta e para outros propósitos, como descritos em nossa [privacy_policy]', 'yes'),
(213, 'woocommerce_checkout_privacy_policy_text', 'Os seus dados pessoais serão utilizados para processar a sua compra, apoiar a sua experiência em todo este site e para outros fins descritos na nossa [privacy_policy]', 'yes'),
(214, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:6:"months";}', 'no'),
(215, 'woocommerce_trash_pending_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:4:"days";}', 'no'),
(216, 'woocommerce_trash_failed_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:4:"days";}', 'no'),
(217, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:4:"days";}', 'no'),
(218, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:"number";s:0:"";s:4:"unit";s:6:"months";}', 'no'),
(219, 'woocommerce_email_from_name', 'Pollev', 'no'),
(220, 'woocommerce_email_from_address', 'pollev@pollev.com', 'no'),
(221, 'woocommerce_email_header_image', 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/logo.png', 'no'),
(222, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(223, 'woocommerce_email_base_color', '#dc1879', 'no'),
(224, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(225, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(226, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(227, 'woocommerce_cart_page_id', '6', 'yes'),
(228, 'woocommerce_checkout_page_id', '7', 'yes'),
(229, 'woocommerce_myaccount_page_id', '8', 'yes'),
(230, 'woocommerce_terms_page_id', '', 'no'),
(231, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(232, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(233, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(234, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(235, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(236, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(237, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(238, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(239, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(240, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(241, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(242, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(243, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(244, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(245, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(246, 'woocommerce_api_enabled', 'no', 'yes'),
(247, 'woocommerce_single_image_width', '600', 'yes'),
(248, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(249, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(250, 'woocommerce_demo_store', 'no', 'no'),
(251, 'woocommerce_permalinks', 'a:5:{s:12:"product_base";s:7:"produto";s:13:"category_base";s:17:"categoria-produto";s:8:"tag_base";s:11:"produto-tag";s:14:"attribute_base";s:0:"";s:22:"use_verbose_page_rules";b:0;}', 'yes'),
(252, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(253, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(256, 'default_product_cat', '15', 'yes'),
(259, 'woocommerce_version', '3.4.4', 'yes'),
(260, 'woocommerce_db_version', '3.4.4', 'yes'),
(261, 'woocommerce_admin_notices', 'a:1:{i:1;s:20:"no_secure_connection";}', 'yes'),
(262, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(263, 'widget_woocommerce_widget_cart', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(264, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(265, 'widget_woocommerce_layered_nav', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(266, 'widget_woocommerce_price_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(267, 'widget_woocommerce_product_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(268, 'widget_woocommerce_product_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(269, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(270, 'widget_woocommerce_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(271, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(272, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(273, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(274, 'widget_woocommerce_rating_filter', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(288, 'woocommerce_cheque_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(289, 'woocommerce_bacs_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(287, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:"reroute_requests";b:0;s:5:"email";s:21:"asjesus1994@gmail.com";}', 'yes'),
(284, 'woocommerce_product_type', 'physical', 'yes'),
(285, 'woocommerce_allow_tracking', 'no', 'yes'),
(290, 'woocommerce_cod_settings', 'a:1:{s:7:"enabled";s:2:"no";}', 'yes'),
(291, 'woocommerce_admin_notice_ppec_paypal_install_error', 'WooCommerce PayPal Express Checkout Gateway não pode ser instalado (Ocorreu um erro inesperado. Algo pode estar errado com o WordPress.org ou com a configuração deste servidor. Se os problemas persistirem, busque ajuda no <a href="https://br.wordpress.org/support/">fórum de suporte</a>.). <a href="http://localhost/wordpress/wp-admin/index.php?wc-install-plugin-redirect=woocommerce-gateway-paypal-express-checkout">Instale manualmente clicando aqui.</a>', 'yes'),
(488, 'nav_menu_options', 'a:1:{s:8:"auto_add";a:0:{}}', 'yes'),
(311, '_transient_product_query-transient-version', '1543071184', 'yes'),
(314, '_transient_product-transient-version', '1543071184', 'yes'),
(1116, 'woocommerce_correios-sedex_4_settings', 'a:24:{s:7:"enabled";s:3:"yes";s:5:"title";s:5:"SEDEX";s:16:"behavior_options";s:0:"";s:15:"origin_postcode";s:10:"41.260-190";s:17:"shipping_class_id";s:2:"-1";s:18:"show_delivery_time";s:2:"no";s:15:"additional_time";s:1:"0";s:3:"fee";s:4:"2.50";s:17:"optional_services";s:0:"";s:14:"receipt_notice";s:2:"no";s:9:"own_hands";s:2:"no";s:13:"declare_value";s:3:"yes";s:15:"service_options";s:0:"";s:11:"custom_code";s:0:"";s:12:"service_type";s:12:"conventional";s:5:"login";s:0:"";s:8:"password";s:0:"";s:16:"package_standard";s:0:"";s:14:"minimum_height";s:1:"2";s:13:"minimum_width";s:2:"11";s:14:"minimum_length";s:2:"16";s:12:"extra_weight";s:1:"0";s:7:"testing";s:0:"";s:5:"debug";s:2:"no";}', 'yes'),
(331, 'category_children', 'a:0:{}', 'yes'),
(437, 'theme_mods_storefront', 'a:6:{i:0;b:0;s:18:"nav_menu_locations";a:3:{s:7:"primary";i:17;s:9:"secondary";i:0;s:8:"handheld";i:0;}s:18:"custom_css_post_id";i:-1;s:11:"custom_logo";i:19;s:34:"storefront_header_background_color";s:7:"#f4f4f4";s:12:"header_image";s:13:"remove-header";}', 'yes'),
(377, 'current_theme', 'Storefront', 'yes'),
(378, 'theme_mods_envo-multipurpose', 'a:4:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1534368731;s:4:"data";a:6:{s:19:"wp_inactive_widgets";a:0:{}s:31:"envo-multipurpose-right-sidebar";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:29:"envo-multipurpose-header-area";a:0:{}s:27:"envo-multipurpose-menu-area";a:0:{}s:29:"envo-multipurpose-footer-area";a:0:{}s:31:"envo-multipurpose-homepage-area";a:0:{}}}}', 'yes'),
(379, 'theme_switched', '', 'yes'),
(380, 'widget_envo-multipurpose-extended-recent-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(381, 'widget_envo-multipurpose-popular-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(382, 'widget_envo-multipurpose-content-widget-slider', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(383, 'widget_envo-multipurpose-content-widget-services', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(384, 'widget_envo-multipurpose-widget-static-content', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(385, 'widget_envo-multipurpose-content-widget-woocommerce-products', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(386, 'widget_envo-multipurpose-content-widget-woocommerce-categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(387, 'widget_envo-multipurpose-widget-blog-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(439, 'woocommerce_catalog_rows', '4', 'yes'),
(388, 'woocommerce_maybe_regenerate_images_hash', '27acde77266b4d2a3491118955cb3f66', 'yes'),
(409, 'woocommerce_paypal_settings', 'a:23:{s:7:"enabled";s:2:"no";s:5:"title";s:6:"PayPal";s:11:"description";s:107:"Pague com PayPal; você pode pagar com o seu cartão de crédito caso você não tenha uma conta no PayPal.";s:5:"email";s:21:"asjesus1994@gmail.com";s:8:"advanced";s:0:"";s:8:"testmode";s:2:"no";s:5:"debug";s:2:"no";s:16:"ipn_notification";s:3:"yes";s:14:"receiver_email";s:21:"asjesus1994@gmail.com";s:14:"identity_token";s:0:"";s:14:"invoice_prefix";s:3:"WC-";s:13:"send_shipping";s:3:"yes";s:16:"address_override";s:2:"no";s:13:"paymentaction";s:4:"sale";s:10:"page_style";s:0:"";s:9:"image_url";s:0:"";s:11:"api_details";s:0:"";s:12:"api_username";s:0:"";s:12:"api_password";s:0:"";s:13:"api_signature";s:0:"";s:20:"sandbox_api_username";s:0:"";s:20:"sandbox_api_password";s:0:"";s:21:"sandbox_api_signature";s:0:"";}', 'yes'),
(1076, 'wcbcf_settings', 'a:8:{s:11:"person_type";s:1:"2";s:13:"birthdate_sex";s:1:"1";s:10:"cell_phone";s:1:"1";s:9:"mailcheck";s:1:"1";s:11:"maskedinput";s:1:"1";s:15:"addresscomplete";s:1:"1";s:12:"validate_cpf";s:1:"1";s:13:"validate_cnpj";s:1:"1";}', 'yes'),
(1077, 'wcbcf_version', '3.6.1', 'yes'),
(440, 'woocommerce_catalog_columns', '4', 'yes'),
(438, 'storefront_nux_fresh_site', '0', 'yes'),
(451, 'storefront_nux_dismissed', '1', 'yes'),
(452, 'storefront_nux_guided_tour', '1', 'yes'),
(487, 'woocommerce_default_catalog_orderby', 'popularity', 'yes'),
(697, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(1525, 'dismissed_update_core', 'a:1:{s:11:"4.9.8|pt_BR";b:1;}', 'no'),
(932, 'new_admin_email', 'asjesus1994@gmail.com', 'yes'),
(1109, 'woocommerce_correios-pac_3_settings', 'a:24:{s:7:"enabled";s:3:"yes";s:5:"title";s:3:"PAC";s:16:"behavior_options";s:0:"";s:15:"origin_postcode";s:10:"41.260-190";s:17:"shipping_class_id";s:2:"-1";s:18:"show_delivery_time";s:3:"yes";s:15:"additional_time";s:1:"1";s:3:"fee";s:4:"2.50";s:17:"optional_services";s:0:"";s:14:"receipt_notice";s:2:"no";s:9:"own_hands";s:2:"no";s:13:"declare_value";s:3:"yes";s:15:"service_options";s:0:"";s:11:"custom_code";s:0:"";s:12:"service_type";s:12:"conventional";s:5:"login";s:0:"";s:8:"password";s:0:"";s:16:"package_standard";s:0:"";s:14:"minimum_height";s:1:"2";s:13:"minimum_width";s:2:"11";s:14:"minimum_length";s:2:"16";s:12:"extra_weight";s:1:"0";s:7:"testing";s:0:"";s:5:"debug";s:2:"no";}', 'yes'),
(2074, '_transient_timeout_wc_product_loop73ac1536068923', '1545527490', 'no'),
(2075, '_transient_wc_product_loop73ac1536068923', 'O:8:"stdClass":5:{s:3:"ids";a:4:{i:0;i:49;i:1;i:85;i:2;i:47;i:3;i:40;}s:5:"total";i:4;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no'),
(2081, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:3:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:63:"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.zip";s:6:"locale";s:5:"pt_BR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:63:"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"5.0";s:7:"version";s:3:"5.0";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"5.0";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:57:"https://downloads.wordpress.org/release/wordpress-5.0.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:57:"https://downloads.wordpress.org/release/wordpress-5.0.zip";s:10:"no_content";s:68:"https://downloads.wordpress.org/release/wordpress-5.0-no-content.zip";s:11:"new_bundled";s:69:"https://downloads.wordpress.org/release/wordpress-5.0-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"5.0";s:7:"version";s:3:"5.0";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"5.0";s:15:"partial_version";s:0:"";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:63:"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.zip";s:6:"locale";s:5:"pt_BR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:63:"https://downloads.wordpress.org/release/pt_BR/wordpress-5.0.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"5.0";s:7:"version";s:3:"5.0";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"5.0";s:15:"partial_version";s:0:"";s:9:"new_files";s:1:"1";}}s:12:"last_checked";i:1544666050;s:15:"version_checked";s:5:"4.9.8";s:12:"translations";a:0:{}}', 'no'),
(2072, '_transient_timeout_wc_product_loope5e11536068923', '1545527490', 'no'),
(2073, '_transient_wc_product_loope5e11536068923', 'O:8:"stdClass":5:{s:3:"ids";a:3:{i:0;i:49;i:1;i:47;i:2;i:9;}s:5:"total";i:3;s:11:"total_pages";i:1;s:8:"per_page";i:4;s:12:"current_page";i:1;}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM AUTO_INCREMENT=899 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(685, 109, '_completed_date', ''),
(2, 3, '_wp_page_template', 'default'),
(3, 9, '_wc_review_count', '0'),
(4, 9, '_wc_rating_count', 'a:0:{}'),
(5, 9, '_wc_average_rating', '0'),
(6, 9, '_edit_last', '1'),
(7, 9, '_edit_lock', '1534392872:1'),
(712, 109, '_order_shipping', '27.83'),
(671, 3, '_edit_last', '1'),
(711, 109, '_cart_discount_tax', '0'),
(710, 109, '_cart_discount', '0'),
(709, 109, '_order_currency', 'BRL'),
(693, 109, '_billing_address_2', 'casa'),
(694, 109, '_billing_city', 'Terra Nova'),
(695, 109, '_billing_state', 'BA'),
(696, 109, '_billing_postcode', '44270-000'),
(697, 109, '_billing_country', 'BR'),
(698, 109, '_billing_email', 'adriano.jesus@continuumweb.com.br'),
(699, 109, '_billing_phone', '-71- 9923-1995'),
(700, 109, '_shipping_first_name', 'Adriano'),
(701, 109, '_shipping_last_name', 'da Silva de Jesus'),
(702, 109, '_shipping_company', ''),
(703, 109, '_shipping_address_1', 'Rua das Flores'),
(704, 109, '_shipping_address_2', 'casa'),
(705, 109, '_shipping_city', 'Terra Nova'),
(706, 109, '_shipping_state', 'BA'),
(707, 109, '_shipping_postcode', '44270-000'),
(708, 109, '_shipping_country', 'BR'),
(10, 9, '_sku', ''),
(11, 9, '_regular_price', '2000'),
(12, 9, '_sale_price', '1500'),
(13, 9, '_sale_price_dates_from', ''),
(14, 9, '_sale_price_dates_to', ''),
(15, 9, 'total_sales', '0'),
(16, 9, '_tax_status', 'taxable'),
(17, 9, '_tax_class', ''),
(18, 9, '_manage_stock', 'no'),
(19, 9, '_backorders', 'no'),
(20, 9, '_sold_individually', 'no'),
(21, 9, '_weight', ''),
(22, 9, '_length', ''),
(23, 9, '_width', ''),
(24, 9, '_height', ''),
(25, 9, '_upsell_ids', 'a:0:{}'),
(26, 9, '_crosssell_ids', 'a:0:{}'),
(27, 9, '_purchase_note', ''),
(28, 9, '_default_attributes', 'a:0:{}'),
(29, 9, '_virtual', 'no'),
(30, 9, '_downloadable', 'no'),
(31, 9, '_product_image_gallery', '50,48,51'),
(32, 9, '_download_limit', '-1'),
(33, 9, '_download_expiry', '-1'),
(34, 9, '_stock', NULL),
(35, 9, '_stock_status', 'instock'),
(36, 9, '_product_version', '3.4.4'),
(37, 9, '_price', '1500'),
(38, 6, '_edit_lock', '1534735761:1'),
(668, 106, '_wp_attached_file', '2018/08/Banner-4-1.jpg'),
(669, 106, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1906;s:6:"height";i:421;s:4:"file";s:22:"2018/08/Banner-4-1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Banner-4-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"Banner-4-1-300x66.jpg";s:5:"width";i:300;s:6:"height";i:66;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"Banner-4-1-768x170.jpg";s:5:"width";i:768;s:6:"height";i:170;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"Banner-4-1-1024x226.jpg";s:5:"width";i:1024;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:22:"Banner-4-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:21:"Banner-4-1-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:22:"Banner-4-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:22:"Banner-4-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:21:"Banner-4-1-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:22:"Banner-4-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(666, 105, '_wp_attached_file', '2018/08/Banner-3.jpg'),
(667, 105, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1906;s:6:"height";i:421;s:4:"file";s:20:"2018/08/Banner-3.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"Banner-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"Banner-3-300x66.jpg";s:5:"width";i:300;s:6:"height";i:66;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"Banner-3-768x170.jpg";s:5:"width";i:768;s:6:"height";i:170;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"Banner-3-1024x226.jpg";s:5:"width";i:1024;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:20:"Banner-3-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:19:"Banner-3-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:20:"Banner-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"Banner-3-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"Banner-3-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"Banner-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(662, 103, '_wp_attached_file', '2018/08/Banner-1.jpg'),
(663, 103, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1906;s:6:"height";i:421;s:4:"file";s:20:"2018/08/Banner-1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"Banner-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"Banner-1-300x66.jpg";s:5:"width";i:300;s:6:"height";i:66;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"Banner-1-768x170.jpg";s:5:"width";i:768;s:6:"height";i:170;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"Banner-1-1024x226.jpg";s:5:"width";i:1024;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:20:"Banner-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:19:"Banner-1-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:20:"Banner-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"Banner-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"Banner-1-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"Banner-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(664, 104, '_wp_attached_file', '2018/08/Banner-2-1.jpg'),
(665, 104, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1906;s:6:"height";i:421;s:4:"file";s:22:"2018/08/Banner-2-1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Banner-2-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"Banner-2-1-300x66.jpg";s:5:"width";i:300;s:6:"height";i:66;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"Banner-2-1-768x170.jpg";s:5:"width";i:768;s:6:"height";i:170;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"Banner-2-1-1024x226.jpg";s:5:"width";i:1024;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:22:"Banner-2-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:21:"Banner-2-1-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:22:"Banner-2-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:22:"Banner-2-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:21:"Banner-2-1-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:22:"Banner-2-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(45, 9, '_thumbnail_id', '51'),
(46, 16, '_wp_page_template', 'template-homepage.php'),
(691, 109, '_billing_company', ''),
(48, 16, '_customize_changeset_uuid', '7574e6a6-e5a0-420f-99c7-86949b583edf'),
(692, 109, '_billing_address_1', 'Rua das Flores'),
(681, 109, '_customer_ip_address', '200.149.164.50'),
(51, 19, '_wp_attached_file', '2018/08/logo.png'),
(52, 19, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:233;s:6:"height";i:68;s:4:"file";s:16:"2018/08/logo.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"logo-150x68.png";s:5:"width";i:150;s:6:"height";i:68;s:9:"mime-type";s:9:"image/png";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:15:"logo-100x68.png";s:5:"width";i:100;s:6:"height";i:68;s:9:"mime-type";s:9:"image/png";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:15:"logo-100x68.png";s:5:"width";i:100;s:6:"height";i:68;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(689, 109, '_billing_first_name', 'Adriano'),
(57, 16, '_edit_lock', '1536088878:1'),
(690, 109, '_billing_last_name', 'da Silva de Jesus'),
(60, 23, '_wp_attached_file', '2018/08/Banner-2.jpg'),
(61, 23, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1906;s:6:"height";i:421;s:4:"file";s:20:"2018/08/Banner-2.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"Banner-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"Banner-2-300x66.jpg";s:5:"width";i:300;s:6:"height";i:66;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"Banner-2-768x170.jpg";s:5:"width";i:768;s:6:"height";i:170;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"Banner-2-1024x226.jpg";s:5:"width";i:1024;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:20:"Banner-2-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:19:"Banner-2-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:20:"Banner-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"Banner-2-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"Banner-2-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"Banner-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(62, 16, '_edit_last', '1'),
(63, 16, '_thumbnail_id', '105'),
(64, 5, '_edit_lock', '1534384387:1'),
(65, 5, '_edit_last', '1'),
(67, 32, '_wp_attached_file', '2018/08/BANNER_QUADRADOS2.jpg'),
(68, 32, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1750;s:6:"height";i:1665;s:4:"file";s:29:"2018/08/BANNER_QUADRADOS2.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-300x285.jpg";s:5:"width";i:300;s:6:"height";i:285;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-768x731.jpg";s:5:"width";i:768;s:6:"height";i:731;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"BANNER_QUADRADOS2-1024x974.jpg";s:5:"width";i:1024;s:6:"height";i:974;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:29:"BANNER_QUADRADOS2-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(69, 33, '_wp_attached_file', '2018/08/BANNER_QUADRADOS2-1.jpg'),
(70, 33, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1750;s:6:"height";i:1665;s:4:"file";s:31:"2018/08/BANNER_QUADRADOS2-1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-300x285.jpg";s:5:"width";i:300;s:6:"height";i:285;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-768x731.jpg";s:5:"width";i:768;s:6:"height";i:731;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:32:"BANNER_QUADRADOS2-1-1024x974.jpg";s:5:"width";i:1024;s:6:"height";i:974;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:31:"BANNER_QUADRADOS2-1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(71, 34, '_wp_attached_file', '2018/08/BANNER_QUADRADOS3.jpg'),
(72, 34, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1750;s:6:"height";i:1665;s:4:"file";s:29:"2018/08/BANNER_QUADRADOS3.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-300x285.jpg";s:5:"width";i:300;s:6:"height";i:285;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-768x731.jpg";s:5:"width";i:768;s:6:"height";i:731;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"BANNER_QUADRADOS3-1024x974.jpg";s:5:"width";i:1024;s:6:"height";i:974;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:29:"BANNER_QUADRADOS3-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(73, 35, '_wp_attached_file', '2018/08/BANNER_QUADRADOS4.jpg'),
(74, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1750;s:6:"height";i:1665;s:4:"file";s:29:"2018/08/BANNER_QUADRADOS4.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-300x285.jpg";s:5:"width";i:300;s:6:"height";i:285;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-768x731.jpg";s:5:"width";i:768;s:6:"height";i:731;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"BANNER_QUADRADOS4-1024x974.jpg";s:5:"width";i:1024;s:6:"height";i:974;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:29:"BANNER_QUADRADOS4-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS4-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(75, 36, '_wp_attached_file', '2018/08/BANNER_QUADRADOS1.jpg'),
(76, 36, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1750;s:6:"height";i:1665;s:4:"file";s:29:"2018/08/BANNER_QUADRADOS1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-300x285.jpg";s:5:"width";i:300;s:6:"height";i:285;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-768x731.jpg";s:5:"width";i:768;s:6:"height";i:731;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:30:"BANNER_QUADRADOS1-1024x974.jpg";s:5:"width";i:1024;s:6:"height";i:974;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:29:"BANNER_QUADRADOS1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-416x396.jpg";s:5:"width";i:416;s:6:"height";i:396;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:29:"BANNER_QUADRADOS1-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(77, 7, '_edit_lock', '1534382755:1'),
(78, 37, '_wp_attached_file', '2018/08/Banner-4.jpg'),
(79, 37, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1906;s:6:"height";i:421;s:4:"file";s:20:"2018/08/Banner-4.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"Banner-4-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"Banner-4-300x66.jpg";s:5:"width";i:300;s:6:"height";i:66;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"Banner-4-768x170.jpg";s:5:"width";i:768;s:6:"height";i:170;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"Banner-4-1024x226.jpg";s:5:"width";i:1024;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:20:"Banner-4-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:19:"Banner-4-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:20:"Banner-4-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:20:"Banner-4-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"Banner-4-416x92.jpg";s:5:"width";i:416;s:6:"height";i:92;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:20:"Banner-4-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(686, 109, '_date_paid', ''),
(687, 109, '_paid_date', ''),
(688, 109, '_cart_hash', '05af5e741d1cfa7e585e0d36e418bb1e'),
(683, 109, '_created_via', 'checkout'),
(684, 109, '_date_completed', ''),
(682, 109, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.1; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(680, 109, '_transaction_id', ''),
(93, 40, '_wc_review_count', '0'),
(94, 40, '_wc_rating_count', 'a:0:{}'),
(95, 40, '_wc_average_rating', '0'),
(96, 40, '_edit_last', '1'),
(97, 40, '_edit_lock', '1534391526:1'),
(98, 46, '_wp_attached_file', '2018/08/777703172_Detalhes.jpg'),
(99, 46, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:465;s:6:"height";i:465;s:4:"file";s:30:"2018/08/777703172_Detalhes.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:30:"777703172_Detalhes-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:30:"777703172_Detalhes-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:30:"777703172_Detalhes-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:30:"777703172_Detalhes-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:30:"777703172_Detalhes-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:30:"777703172_Detalhes-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:30:"777703172_Detalhes-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:30:"777703172_Detalhes-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(100, 40, '_thumbnail_id', '46'),
(101, 40, '_sku', 'bolsa-preta'),
(102, 40, '_regular_price', '80'),
(103, 40, '_sale_price', ''),
(104, 40, '_sale_price_dates_from', ''),
(105, 40, '_sale_price_dates_to', ''),
(106, 40, 'total_sales', '0'),
(107, 40, '_tax_status', 'taxable'),
(108, 40, '_tax_class', ''),
(109, 40, '_manage_stock', 'yes'),
(110, 40, '_backorders', 'no'),
(111, 40, '_sold_individually', 'yes'),
(112, 40, '_weight', '200'),
(113, 40, '_length', '5'),
(114, 40, '_width', '30'),
(115, 40, '_height', '20'),
(116, 40, '_upsell_ids', 'a:0:{}'),
(117, 40, '_crosssell_ids', 'a:0:{}'),
(118, 40, '_purchase_note', ''),
(119, 40, '_default_attributes', 'a:0:{}'),
(120, 40, '_virtual', 'no'),
(121, 40, '_downloadable', 'no'),
(122, 40, '_product_image_gallery', '46'),
(123, 40, '_download_limit', '-1'),
(124, 40, '_download_expiry', '-1'),
(125, 40, '_stock', '5'),
(126, 40, '_stock_status', 'instock'),
(127, 40, '_product_version', '3.4.4'),
(128, 40, '_price', '80'),
(129, 47, '_wc_review_count', '0'),
(130, 47, '_wc_rating_count', 'a:0:{}'),
(131, 47, '_wc_average_rating', '0'),
(132, 47, '_edit_last', '1'),
(133, 47, '_edit_lock', '1534998383:1'),
(134, 48, '_wp_attached_file', '2018/08/689675-MLA26280173077_112017-O.jpg'),
(135, 48, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:500;s:4:"file";s:42:"2018/08/689675-MLA26280173077_112017-O.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:42:"689675-MLA26280173077_112017-O-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(136, 47, '_thumbnail_id', '48'),
(137, 47, '_sku', 'garrafa-termica'),
(138, 47, '_regular_price', '60'),
(139, 47, '_sale_price', '55'),
(140, 47, '_sale_price_dates_from', ''),
(141, 47, '_sale_price_dates_to', ''),
(142, 47, 'total_sales', '1'),
(143, 47, '_tax_status', 'taxable'),
(144, 47, '_tax_class', ''),
(145, 47, '_manage_stock', 'yes'),
(146, 47, '_backorders', 'no'),
(147, 47, '_sold_individually', 'yes'),
(148, 47, '_weight', '0.300'),
(149, 47, '_length', '25'),
(150, 47, '_width', '10'),
(151, 47, '_height', '10'),
(152, 47, '_upsell_ids', 'a:0:{}'),
(153, 47, '_crosssell_ids', 'a:0:{}'),
(154, 47, '_purchase_note', ''),
(155, 47, '_default_attributes', 'a:0:{}'),
(156, 47, '_virtual', 'no'),
(157, 47, '_downloadable', 'no'),
(158, 47, '_product_image_gallery', '48'),
(159, 47, '_download_limit', '-1'),
(160, 47, '_download_expiry', '-1'),
(161, 47, '_stock', '10'),
(162, 47, '_stock_status', 'instock'),
(163, 47, '_product_version', '3.4.4'),
(164, 47, '_price', '55'),
(165, 49, '_wc_review_count', '0'),
(166, 49, '_wc_rating_count', 'a:0:{}'),
(167, 49, '_wc_average_rating', '0'),
(168, 49, '_edit_last', '1'),
(169, 49, '_edit_lock', '1535213306:1'),
(170, 50, '_wp_attached_file', '2018/08/17126601_224096344726267_8884516374378971136_n.jpg'),
(171, 50, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:480;s:6:"height";i:480;s:4:"file";s:58:"2018/08/17126601_224096344726267_8884516374378971136_n.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:58:"17126601_224096344726267_8884516374378971136_n-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(172, 49, '_thumbnail_id', '50'),
(173, 49, '_sku', 'bolsa-termica-bege'),
(174, 49, '_regular_price', '40'),
(175, 49, '_sale_price', '35'),
(176, 49, '_sale_price_dates_from', ''),
(177, 49, '_sale_price_dates_to', ''),
(178, 49, 'total_sales', '2'),
(179, 49, '_tax_status', 'taxable'),
(180, 49, '_tax_class', ''),
(181, 49, '_manage_stock', 'yes'),
(182, 49, '_backorders', 'no'),
(183, 49, '_sold_individually', 'yes'),
(184, 49, '_weight', '10'),
(185, 49, '_length', '10'),
(186, 49, '_width', '10'),
(187, 49, '_height', '10'),
(188, 49, '_upsell_ids', 'a:1:{i:0;i:40;}'),
(189, 49, '_crosssell_ids', 'a:1:{i:0;i:47;}'),
(190, 49, '_purchase_note', ''),
(191, 49, '_default_attributes', 'a:0:{}'),
(192, 49, '_virtual', 'no'),
(193, 49, '_downloadable', 'no'),
(194, 49, '_product_image_gallery', '50'),
(195, 49, '_download_limit', '-1'),
(196, 49, '_download_expiry', '-1'),
(197, 49, '_stock', '10'),
(198, 49, '_stock_status', 'instock'),
(199, 49, '_product_version', '3.4.4'),
(200, 49, '_price', '35'),
(201, 51, '_wp_attached_file', '2018/08/bolsa-termica-lancheira-oncinha-chic.jpg'),
(202, 51, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:500;s:4:"file";s:48:"2018/08/bolsa-termica-lancheira-oncinha-chic.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-416x416.jpg";s:5:"width";i:416;s:6:"height";i:416;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:48:"bolsa-termica-lancheira-oncinha-chic-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(534, 84, '_customer_user', '6'),
(533, 84, '_order_key', 'wc_order_5b817100ab3f6'),
(206, 53, '_menu_item_type', 'custom'),
(207, 53, '_menu_item_menu_item_parent', '0'),
(208, 53, '_menu_item_object_id', '53'),
(209, 53, '_menu_item_object', 'custom'),
(210, 53, '_menu_item_target', ''),
(211, 53, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(212, 53, '_menu_item_xfn', ''),
(213, 53, '_menu_item_url', 'http://teste.com'),
(214, 53, '_menu_item_orphaned', '1534625076'),
(677, 109, '_customer_user', '6'),
(216, 55, '_menu_item_type', 'taxonomy'),
(217, 55, '_menu_item_menu_item_parent', '0'),
(218, 55, '_menu_item_object_id', '16'),
(219, 55, '_menu_item_object', 'product_cat'),
(220, 55, '_menu_item_target', ''),
(221, 55, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(222, 55, '_menu_item_xfn', ''),
(223, 55, '_menu_item_url', ''),
(224, 56, '_menu_item_type', 'taxonomy'),
(225, 56, '_menu_item_menu_item_parent', '0'),
(226, 56, '_menu_item_object_id', '19'),
(227, 56, '_menu_item_object', 'product_cat'),
(228, 56, '_menu_item_target', ''),
(229, 56, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(230, 56, '_menu_item_xfn', ''),
(231, 56, '_menu_item_url', ''),
(232, 57, '_menu_item_type', 'taxonomy'),
(233, 57, '_menu_item_menu_item_parent', '0'),
(234, 57, '_menu_item_object_id', '20'),
(235, 57, '_menu_item_object', 'product_cat'),
(236, 57, '_menu_item_target', ''),
(237, 57, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(238, 57, '_menu_item_xfn', ''),
(239, 57, '_menu_item_url', ''),
(240, 58, '_menu_item_type', 'taxonomy'),
(241, 58, '_menu_item_menu_item_parent', '0'),
(242, 58, '_menu_item_object_id', '18'),
(243, 58, '_menu_item_object', 'product_cat'),
(244, 58, '_menu_item_target', ''),
(245, 58, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(246, 58, '_menu_item_xfn', ''),
(247, 58, '_menu_item_url', ''),
(248, 59, '_menu_item_type', 'taxonomy'),
(249, 59, '_menu_item_menu_item_parent', '0'),
(250, 59, '_menu_item_object_id', '21'),
(251, 59, '_menu_item_object', 'product_cat'),
(252, 59, '_menu_item_target', ''),
(253, 59, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(254, 59, '_menu_item_xfn', ''),
(255, 59, '_menu_item_url', ''),
(679, 109, '_payment_method_title', 'PagSeguro'),
(678, 109, '_payment_method', 'pagseguro'),
(676, 109, '_order_key', 'wc_order_5bbbec341dd8d'),
(261, 62, '_menu_item_type', 'taxonomy'),
(262, 62, '_menu_item_menu_item_parent', '55'),
(263, 62, '_menu_item_object_id', '23'),
(264, 62, '_menu_item_object', 'product_cat'),
(265, 62, '_menu_item_target', ''),
(266, 62, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(267, 62, '_menu_item_xfn', ''),
(268, 62, '_menu_item_url', ''),
(270, 63, '_menu_item_type', 'taxonomy'),
(271, 63, '_menu_item_menu_item_parent', '55'),
(272, 63, '_menu_item_object_id', '22'),
(273, 63, '_menu_item_object', 'product_cat'),
(274, 63, '_menu_item_target', ''),
(275, 63, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(276, 63, '_menu_item_xfn', ''),
(277, 63, '_menu_item_url', ''),
(650, 92, '_edit_lock', '1535549272:1'),
(280, 67, '_order_key', 'wc_order_5b7c9f72ba62c'),
(281, 67, '_customer_user', '2'),
(282, 67, '_payment_method', 'paypal'),
(283, 67, '_payment_method_title', 'PayPal'),
(284, 67, '_transaction_id', ''),
(285, 67, '_customer_ip_address', '177.134.179.117'),
(286, 67, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36'),
(287, 67, '_created_via', 'checkout'),
(288, 67, '_date_completed', ''),
(289, 67, '_completed_date', ''),
(290, 67, '_date_paid', ''),
(291, 67, '_paid_date', ''),
(292, 67, '_cart_hash', 'ebab73d66bc637fb086b26d40141c17c'),
(293, 67, '_billing_first_name', 'Jussana'),
(294, 67, '_billing_last_name', 'Jambeiro'),
(295, 67, '_billing_company', ''),
(296, 67, '_billing_address_1', 'Dr Americo Silva 96'),
(297, 67, '_billing_address_2', ''),
(298, 67, '_billing_city', 'salvador'),
(299, 67, '_billing_state', 'BA'),
(300, 67, '_billing_postcode', '40140-490'),
(301, 67, '_billing_country', 'BR'),
(302, 67, '_billing_email', 'jussanajambeiro@gmail.com'),
(303, 67, '_billing_phone', '71 999846930'),
(304, 67, '_shipping_first_name', 'Jussana'),
(305, 67, '_shipping_last_name', 'Jambeiro'),
(306, 67, '_shipping_company', ''),
(307, 67, '_shipping_address_1', 'Dr Americo Silva 96'),
(308, 67, '_shipping_address_2', ''),
(309, 67, '_shipping_city', 'salvador'),
(310, 67, '_shipping_state', 'BA'),
(311, 67, '_shipping_postcode', '40140-490'),
(312, 67, '_shipping_country', 'BR'),
(313, 67, '_order_currency', 'BRL'),
(314, 67, '_cart_discount', '0'),
(315, 67, '_cart_discount_tax', '0'),
(316, 67, '_order_shipping', '10.00'),
(317, 67, '_order_shipping_tax', '0'),
(318, 67, '_order_tax', '0'),
(319, 67, '_order_total', '65.00'),
(320, 67, '_order_version', '3.4.4'),
(321, 67, '_prices_include_tax', 'no'),
(322, 67, '_billing_address_index', 'Jussana Jambeiro  Dr Americo Silva 96  salvador BA 40140-490 BR jussanajambeiro@gmail.com 71 999846930'),
(323, 67, '_shipping_address_index', 'Jussana Jambeiro  Dr Americo Silva 96  salvador BA 40140-490 BR'),
(718, 109, '_billing_address_index', 'Adriano da Silva de Jesus  Rua das Flores casa Terra Nova BA 44270-000 BR adriano.jesus@continuumweb.com.br -71- 9923-1995 03 Centro'),
(717, 109, '_prices_include_tax', 'no'),
(716, 109, '_order_version', '3.4.4'),
(328, 70, '_order_key', 'wc_order_5b7e2087566bb'),
(329, 70, '_customer_user', '0'),
(330, 70, '_payment_method', 'pagseguro'),
(331, 70, '_payment_method_title', 'PagSeguro'),
(332, 70, '_transaction_id', ''),
(333, 70, '_customer_ip_address', '177.99.77.239'),
(334, 70, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36'),
(335, 70, '_created_via', 'checkout'),
(336, 70, '_date_completed', ''),
(337, 70, '_completed_date', ''),
(338, 70, '_date_paid', ''),
(339, 70, '_paid_date', ''),
(340, 70, '_cart_hash', '6f3e3db7507aac75e159f1e5850e7061'),
(341, 70, '_billing_first_name', 'Adriano'),
(342, 70, '_billing_last_name', 'da Silva de Jesus'),
(343, 70, '_billing_company', ''),
(344, 70, '_billing_address_1', 'Rua Gilberto Gil'),
(345, 70, '_billing_address_2', 'casa'),
(346, 70, '_billing_city', 'Salvador'),
(347, 70, '_billing_state', 'BA'),
(348, 70, '_billing_postcode', '41260-190'),
(349, 70, '_billing_country', 'BR'),
(350, 70, '_billing_email', 'asjesus1994@gmail.com'),
(351, 70, '_billing_phone', '-71- 98342-5212'),
(352, 70, '_shipping_first_name', 'Adriano'),
(353, 70, '_shipping_last_name', 'da Silva de Jesus'),
(354, 70, '_shipping_company', ''),
(355, 70, '_shipping_address_1', 'Rua Gilberto Gil'),
(356, 70, '_shipping_address_2', 'casa'),
(357, 70, '_shipping_city', 'Salvador'),
(358, 70, '_shipping_state', 'BA'),
(359, 70, '_shipping_postcode', '41260-190'),
(360, 70, '_shipping_country', 'BR'),
(361, 70, '_order_currency', 'BRL'),
(362, 70, '_cart_discount', '0'),
(363, 70, '_cart_discount_tax', '0'),
(364, 70, '_order_shipping', '10.00'),
(365, 70, '_order_shipping_tax', '0'),
(366, 70, '_order_tax', '0'),
(367, 70, '_order_total', '45.00'),
(368, 70, '_order_version', '3.4.4'),
(369, 70, '_prices_include_tax', 'no'),
(370, 70, '_billing_address_index', 'Adriano da Silva de Jesus  Rua Gilberto Gil casa Salvador BA 41260-190 BR asjesus1994@gmail.com -71- 98342-5212 23 Canabrava'),
(371, 70, '_shipping_address_index', 'Adriano da Silva de Jesus  Rua Gilberto Gil casa Salvador BA 41260-190 BR 23 Canabrava'),
(372, 70, '_billing_cpf', '063.075.645-71'),
(373, 70, '_billing_birthdate', '29/03/1995'),
(374, 70, '_billing_sex', 'Masculino'),
(375, 70, '_billing_number', '23'),
(376, 70, '_billing_neighborhood', 'Canabrava'),
(377, 70, '_billing_cellphone', ''),
(378, 70, '_shipping_number', '23'),
(379, 70, '_shipping_neighborhood', 'Canabrava'),
(380, 67, '_edit_lock', '1534993757:1'),
(381, 70, '_edit_lock', '1539750590:1'),
(715, 109, '_order_total', '77.73'),
(714, 109, '_order_tax', '0'),
(713, 109, '_order_shipping_tax', '0'),
(386, 47, '_product_attributes', 'a:1:{s:11:"pa_material";a:6:{s:4:"name";s:11:"pa_material";s:5:"value";s:0:"";s:8:"position";i:1;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(387, 73, '_menu_item_type', 'taxonomy'),
(388, 73, '_menu_item_menu_item_parent', '56'),
(389, 73, '_menu_item_object_id', '23'),
(390, 73, '_menu_item_object', 'product_cat'),
(391, 73, '_menu_item_target', ''),
(392, 73, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(393, 73, '_menu_item_xfn', ''),
(394, 73, '_menu_item_url', ''),
(465, 82, '_customer_ip_address', '187.105.68.6'),
(396, 74, '_menu_item_type', 'taxonomy'),
(397, 74, '_menu_item_menu_item_parent', '56'),
(398, 74, '_menu_item_object_id', '22'),
(399, 74, '_menu_item_object', 'product_cat'),
(400, 74, '_menu_item_target', ''),
(401, 74, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(402, 74, '_menu_item_xfn', ''),
(403, 74, '_menu_item_url', ''),
(464, 82, '_transaction_id', ''),
(405, 75, '_menu_item_type', 'taxonomy'),
(406, 75, '_menu_item_menu_item_parent', '57'),
(407, 75, '_menu_item_object_id', '23'),
(408, 75, '_menu_item_object', 'product_cat'),
(409, 75, '_menu_item_target', ''),
(410, 75, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(411, 75, '_menu_item_xfn', ''),
(412, 75, '_menu_item_url', ''),
(463, 82, '_payment_method_title', 'PagSeguro'),
(414, 76, '_menu_item_type', 'taxonomy'),
(415, 76, '_menu_item_menu_item_parent', '57'),
(416, 76, '_menu_item_object_id', '22'),
(417, 76, '_menu_item_object', 'product_cat'),
(418, 76, '_menu_item_target', ''),
(419, 76, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(420, 76, '_menu_item_xfn', ''),
(421, 76, '_menu_item_url', ''),
(462, 82, '_payment_method', 'pagseguro'),
(423, 77, '_menu_item_type', 'taxonomy'),
(424, 77, '_menu_item_menu_item_parent', '58'),
(425, 77, '_menu_item_object_id', '23'),
(426, 77, '_menu_item_object', 'product_cat'),
(427, 77, '_menu_item_target', ''),
(428, 77, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(429, 77, '_menu_item_xfn', ''),
(430, 77, '_menu_item_url', ''),
(461, 82, '_customer_user', '5'),
(432, 78, '_menu_item_type', 'taxonomy'),
(433, 78, '_menu_item_menu_item_parent', '58'),
(434, 78, '_menu_item_object_id', '22'),
(435, 78, '_menu_item_object', 'product_cat'),
(436, 78, '_menu_item_target', ''),
(437, 78, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(438, 78, '_menu_item_xfn', ''),
(439, 78, '_menu_item_url', ''),
(441, 79, '_menu_item_type', 'taxonomy'),
(442, 79, '_menu_item_menu_item_parent', '59'),
(443, 79, '_menu_item_object_id', '23'),
(444, 79, '_menu_item_object', 'product_cat'),
(445, 79, '_menu_item_target', ''),
(446, 79, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(447, 79, '_menu_item_xfn', ''),
(448, 79, '_menu_item_url', ''),
(460, 82, '_order_key', 'wc_order_5b7f218944703'),
(450, 80, '_menu_item_type', 'taxonomy'),
(451, 80, '_menu_item_menu_item_parent', '59'),
(452, 80, '_menu_item_object_id', '22'),
(453, 80, '_menu_item_object', 'product_cat'),
(454, 80, '_menu_item_target', ''),
(455, 80, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(456, 80, '_menu_item_xfn', ''),
(457, 80, '_menu_item_url', ''),
(670, 3, '_edit_lock', '1536043053:1'),
(466, 82, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.3; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36'),
(467, 82, '_created_via', 'checkout'),
(468, 82, '_date_completed', ''),
(469, 82, '_completed_date', ''),
(470, 82, '_date_paid', ''),
(471, 82, '_paid_date', ''),
(472, 82, '_cart_hash', 'd76d77d5ed43f7ecc3a901e3ec1ef1d8'),
(473, 82, '_billing_first_name', 'Atilano'),
(474, 82, '_billing_last_name', 'Muinhos'),
(475, 82, '_billing_company', ''),
(476, 82, '_billing_address_1', 'Rua Alceu Amoroso Lima'),
(477, 82, '_billing_address_2', 'Sala 618'),
(478, 82, '_billing_city', 'Salvador'),
(479, 82, '_billing_state', 'BA'),
(480, 82, '_billing_postcode', '41820-770'),
(481, 82, '_billing_country', 'BR'),
(482, 82, '_billing_email', 'atilano@yellowmelon.com.br'),
(483, 82, '_billing_phone', '-71- 99984-9797'),
(484, 82, '_shipping_first_name', 'Atilano'),
(485, 82, '_shipping_last_name', 'Muinhos'),
(486, 82, '_shipping_company', ''),
(487, 82, '_shipping_address_1', 'Rua Alceu Amoroso Lima'),
(488, 82, '_shipping_address_2', 'Sala 618'),
(489, 82, '_shipping_city', 'Salvador'),
(490, 82, '_shipping_state', 'BA'),
(491, 82, '_shipping_postcode', '41820-770'),
(492, 82, '_shipping_country', 'BR'),
(493, 82, '_order_currency', 'BRL'),
(494, 82, '_cart_discount', '0'),
(495, 82, '_cart_discount_tax', '0'),
(496, 82, '_order_shipping', '20.63'),
(497, 82, '_order_shipping_tax', '0'),
(498, 82, '_order_tax', '0'),
(499, 82, '_order_total', '75.63'),
(500, 82, '_order_version', '3.4.4'),
(501, 82, '_prices_include_tax', 'no'),
(502, 82, '_billing_address_index', 'Atilano Muinhos  Rua Alceu Amoroso Lima Sala 618 Salvador BA 41820-770 BR atilano@yellowmelon.com.br -71- 99984-9797 786 Caminho das Arvores'),
(503, 82, '_shipping_address_index', 'Atilano Muinhos  Rua Alceu Amoroso Lima Sala 618 Salvador BA 41820-770 BR 786 Caminho das Arvores'),
(504, 82, '_billing_cpf', '016.238.285-54'),
(505, 82, '_billing_birthdate', '03/07/1984'),
(506, 82, '_billing_sex', 'Masculino'),
(507, 82, '_billing_number', '786'),
(508, 82, '_billing_neighborhood', 'Caminho das Arvores'),
(509, 82, '_billing_cellphone', ''),
(510, 82, '_shipping_number', '786'),
(511, 82, '_shipping_neighborhood', 'Caminho das Arvores'),
(527, 82, '_wc_pagseguro_payment_data', 'a:4:{s:4:"type";i:2;s:6:"method";s:16:"Boleto Santander";s:12:"installments";s:1:"1";s:4:"link";s:148:"https://pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=43bf62879a8583c02f494f736d6f0ccba712e4daa133d4db0c085ea4c4204bd871337e6e3eaa9627";}'),
(525, 82, 'Parcelas', '1'),
(526, 82, 'URL de pagamento.', 'https://pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=43bf62879a8583c02f494f736d6f0ccba712e4daa133d4db0c085ea4c4204bd871337e6e3eaa9627'),
(521, 82, 'E-mail do comprador', 'atilano@yellowmelon.com.br'),
(522, 82, 'Nome do comprador', 'Atilano Muinhos'),
(523, 82, 'Tipo de pagamento', 'Boleto'),
(524, 82, 'Método de pagamento', 'Boleto Santander'),
(519, 82, '_recorded_sales', 'yes'),
(520, 82, '_recorded_coupon_usage_counts', 'yes'),
(528, 83, '_wc_review_count', '0');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(529, 83, '_wc_rating_count', 'a:0:{}'),
(530, 83, '_wc_average_rating', '0'),
(531, 83, '_edit_lock', '1535059155:1'),
(532, 83, '_edit_last', '1'),
(535, 84, '_payment_method', 'pagseguro'),
(536, 84, '_payment_method_title', 'PagSeguro'),
(537, 84, '_transaction_id', ''),
(538, 84, '_customer_ip_address', '187.105.68.6'),
(539, 84, '_customer_user_agent', 'mozilla/5.0 (windows nt 6.3; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/68.0.3440.106 safari/537.36'),
(540, 84, '_created_via', 'checkout'),
(541, 84, '_date_completed', '1535210965'),
(542, 84, '_completed_date', '2018-08-25 12:29:25'),
(543, 84, '_date_paid', ''),
(544, 84, '_paid_date', ''),
(545, 84, '_cart_hash', '5a08418f485e6145f5d6c6923c552257'),
(546, 84, '_billing_first_name', 'Adriano'),
(547, 84, '_billing_last_name', 'da Silva de Jesus'),
(548, 84, '_billing_company', ''),
(549, 84, '_billing_address_1', 'Rua das Flores'),
(550, 84, '_billing_address_2', 'casa'),
(551, 84, '_billing_city', 'Terra Nova'),
(552, 84, '_billing_state', 'BA'),
(553, 84, '_billing_postcode', '44270-000'),
(554, 84, '_billing_country', 'BR'),
(555, 84, '_billing_email', 'adriano.jesus@continuumweb.com.br'),
(556, 84, '_billing_phone', '-71- 9923-1995'),
(557, 84, '_shipping_first_name', 'Adriano'),
(558, 84, '_shipping_last_name', 'da Silva de Jesus'),
(559, 84, '_shipping_company', ''),
(560, 84, '_shipping_address_1', 'Rua das Flores'),
(561, 84, '_shipping_address_2', 'casa'),
(562, 84, '_shipping_city', 'Terra Nova'),
(563, 84, '_shipping_state', 'BA'),
(564, 84, '_shipping_postcode', '44270-000'),
(565, 84, '_shipping_country', 'BR'),
(566, 84, '_order_currency', 'BRL'),
(567, 84, '_cart_discount', '0'),
(568, 84, '_cart_discount_tax', '0'),
(569, 84, '_order_shipping', '39.83'),
(570, 84, '_order_shipping_tax', '0'),
(571, 84, '_order_tax', '0'),
(572, 84, '_order_total', '74.83'),
(573, 84, '_order_version', '3.4.4'),
(574, 84, '_prices_include_tax', 'no'),
(575, 84, '_billing_address_index', 'Adriano da Silva de Jesus  Rua das Flores casa Terra Nova BA 44270-000 BR adriano.jesus@continuumweb.com.br -71- 9923-1995 03 Centro'),
(576, 84, '_shipping_address_index', 'Adriano da Silva de Jesus  Rua das Flores casa Terra Nova BA 44270-000 BR 03 Centro'),
(608, 84, '_billing_cellphone', ''),
(607, 84, '_billing_neighborhood', 'Centro'),
(606, 84, '_billing_number', '03'),
(605, 84, '_billing_sex', 'Masculino'),
(604, 84, '_billing_birthdate', '29/03/1995'),
(603, 84, '_billing_cpf', '063.075.645-71'),
(600, 84, '_wc_pagseguro_payment_data', 'a:4:{s:4:"type";i:2;s:6:"method";s:16:"Boleto Santander";s:12:"installments";s:1:"1";s:4:"link";s:148:"https://pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=255536ee0fcecb6c47cd8ad5d7f69a3ef7ae58db4c6e5b32cd5c47b74eed132d01b942427ef7d786";}'),
(598, 84, 'Parcelas', '1'),
(599, 84, 'URL de pagamento.', 'https://pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=255536ee0fcecb6c47cd8ad5d7f69a3ef7ae58db4c6e5b32cd5c47b74eed132d01b942427ef7d786'),
(594, 84, 'E-mail do comprador', 'adriano.jesus@continuumweb.com.br'),
(595, 84, 'Nome do comprador', 'Adriano da Silva de Jesus'),
(596, 84, 'Tipo de pagamento', 'Boleto'),
(597, 84, 'Método de pagamento', 'Boleto Santander'),
(592, 84, '_recorded_sales', 'yes'),
(593, 84, '_recorded_coupon_usage_counts', 'yes'),
(601, 84, '_edit_lock', '1539749183:1'),
(602, 84, '_edit_last', '1'),
(609, 84, '_shipping_number', '03'),
(610, 84, '_shipping_neighborhood', 'Centro'),
(611, 84, '_download_permissions_granted', 'yes'),
(612, 82, '_edit_lock', '1536070926:1'),
(613, 85, '_wc_review_count', '0'),
(614, 85, '_wc_rating_count', 'a:0:{}'),
(615, 85, '_wc_average_rating', '0'),
(616, 85, '_edit_lock', '1535213639:1'),
(617, 85, '_edit_last', '1'),
(618, 86, '_wp_attached_file', '2018/08/images.jpg'),
(619, 86, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:225;s:6:"height";i:225;s:4:"file";s:18:"2018/08/images.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"images-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:18:"images-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:18:"images-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(620, 85, '_thumbnail_id', '86'),
(621, 85, '_sku', '0001Teste'),
(622, 85, '_regular_price', '49.90'),
(623, 85, '_sale_price', ''),
(624, 85, '_sale_price_dates_from', ''),
(625, 85, '_sale_price_dates_to', ''),
(626, 85, 'total_sales', '1'),
(627, 85, '_tax_status', 'shipping'),
(628, 85, '_tax_class', ''),
(629, 85, '_manage_stock', 'yes'),
(630, 85, '_backorders', 'no'),
(631, 85, '_sold_individually', 'no'),
(632, 85, '_weight', '0.500'),
(633, 85, '_length', '30'),
(634, 85, '_width', '30'),
(635, 85, '_height', '30'),
(636, 85, '_upsell_ids', 'a:0:{}'),
(637, 85, '_crosssell_ids', 'a:0:{}'),
(638, 85, '_purchase_note', ''),
(639, 85, '_default_attributes', 'a:0:{}'),
(640, 85, '_virtual', 'no'),
(641, 85, '_downloadable', 'no'),
(642, 85, '_product_image_gallery', '50,51,86,48'),
(643, 85, '_download_limit', '-1'),
(644, 85, '_download_expiry', '-1'),
(645, 85, '_stock', '5'),
(646, 85, '_stock_status', 'instock'),
(647, 85, '_product_attributes', 'a:1:{s:11:"pa_material";a:6:{s:4:"name";s:11:"pa_material";s:5:"value";s:0:"";s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(648, 85, '_product_version', '3.4.4'),
(649, 85, '_price', '49.90'),
(651, 92, '_edit_last', '1'),
(652, 92, '_wp_page_template', 'default'),
(653, 95, '_edit_lock', '1535549710:1'),
(654, 95, '_edit_last', '1'),
(655, 95, '_wp_page_template', 'default'),
(656, 98, '_edit_lock', '1535596002:1'),
(657, 98, '_edit_last', '1'),
(658, 98, '_wp_page_template', 'default'),
(659, 100, '_edit_lock', '1535596104:1'),
(660, 100, '_edit_last', '1'),
(661, 100, '_wp_page_template', 'default'),
(719, 109, '_shipping_address_index', 'Adriano da Silva de Jesus  Rua das Flores casa Terra Nova BA 44270-000 BR 03 Centro'),
(720, 109, '_billing_cpf', '063.075.645-71'),
(721, 109, '_billing_birthdate', '29/03/1995'),
(722, 109, '_billing_sex', 'Masculino'),
(723, 109, '_billing_number', '03'),
(724, 109, '_billing_neighborhood', 'Centro'),
(725, 109, '_billing_cellphone', ''),
(726, 109, '_shipping_number', '03'),
(727, 109, '_shipping_neighborhood', 'Centro'),
(728, 109, 'E-mail do comprador', 'adriano.jesus@continuumweb.com.br'),
(729, 109, 'Nome do comprador', 'Adriano da Silva de Jesus'),
(730, 109, 'Tipo de pagamento', 'Boleto'),
(731, 109, 'Método de pagamento', 'Boleto Santander'),
(732, 109, 'Parcelas', '1'),
(733, 109, 'URL de pagamento.', 'https://pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=5243e1e1944099949016a43da6cdeee9bc75307a73b0707c591f43bcca5aef866cbbcce28223cf67'),
(734, 109, '_wc_pagseguro_payment_data', 'a:4:{s:4:"type";i:2;s:6:"method";s:16:"Boleto Santander";s:12:"installments";s:1:"1";s:4:"link";s:148:"https://pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=5243e1e1944099949016a43da6cdeee9bc75307a73b0707c591f43bcca5aef866cbbcce28223cf67";}'),
(735, 109, '_recorded_sales', 'yes'),
(736, 109, '_recorded_coupon_usage_counts', 'yes'),
(737, 109, '_edit_lock', '1539749425:1'),
(738, 112, '_order_key', 'wc_order_5bc6b88d35cae'),
(739, 112, '_customer_user', '6'),
(740, 112, '_payment_method', 'pagseguro'),
(741, 112, '_payment_method_title', 'PagSeguro'),
(742, 112, '_transaction_id', ''),
(743, 112, '_customer_ip_address', '177.207.88.117'),
(744, 112, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(745, 112, '_created_via', 'checkout'),
(746, 112, '_date_completed', ''),
(747, 112, '_completed_date', ''),
(748, 112, '_date_paid', ''),
(749, 112, '_paid_date', ''),
(750, 112, '_cart_hash', '71c4dfd3308174622b014c2bfc2bd7d2'),
(751, 112, '_billing_first_name', 'Adriano'),
(752, 112, '_billing_last_name', 'da Silva de Jesus'),
(753, 112, '_billing_company', ''),
(754, 112, '_billing_address_1', 'Rua das Flores'),
(755, 112, '_billing_address_2', 'casa'),
(756, 112, '_billing_city', 'Terra Nova'),
(757, 112, '_billing_state', 'BA'),
(758, 112, '_billing_postcode', '44270-000'),
(759, 112, '_billing_country', 'BR'),
(760, 112, '_billing_email', 'adriano.jesus@continuumweb.com.br'),
(761, 112, '_billing_phone', '-71- 9923-1995'),
(762, 112, '_shipping_first_name', 'Adriano'),
(763, 112, '_shipping_last_name', 'da Silva de Jesus'),
(764, 112, '_shipping_company', ''),
(765, 112, '_shipping_address_1', 'Rua das Flores'),
(766, 112, '_shipping_address_2', 'casa'),
(767, 112, '_shipping_city', 'Terra Nova'),
(768, 112, '_shipping_state', 'BA'),
(769, 112, '_shipping_postcode', '44270-000'),
(770, 112, '_shipping_country', 'BR'),
(771, 112, '_order_currency', 'BRL'),
(772, 112, '_cart_discount', '0'),
(773, 112, '_cart_discount_tax', '0'),
(774, 112, '_order_shipping', '42.83'),
(775, 112, '_order_shipping_tax', '0'),
(776, 112, '_order_tax', '0'),
(777, 112, '_order_total', '77.83'),
(778, 112, '_order_version', '3.4.4'),
(779, 112, '_prices_include_tax', 'no'),
(780, 112, '_billing_address_index', 'Adriano da Silva de Jesus  Rua das Flores casa Terra Nova BA 44270-000 BR adriano.jesus@continuumweb.com.br -71- 9923-1995 03 Centro'),
(781, 112, '_shipping_address_index', 'Adriano da Silva de Jesus  Rua das Flores casa Terra Nova BA 44270-000 BR 03 Centro'),
(782, 112, '_billing_cpf', '063.075.645-71'),
(783, 112, '_billing_birthdate', '29/03/1995'),
(784, 112, '_billing_sex', 'Masculino'),
(785, 112, '_billing_number', '03'),
(786, 112, '_billing_neighborhood', 'Centro'),
(787, 112, '_billing_cellphone', ''),
(788, 112, '_shipping_number', '03'),
(789, 112, '_shipping_neighborhood', 'Centro'),
(790, 112, 'E-mail do comprador', 'adriano.jesus@continuumweb.com.br'),
(791, 112, 'Nome do comprador', 'Adriano da Silva de Jesus'),
(792, 112, 'Tipo de pagamento', 'Cartão de Crédito'),
(793, 112, 'Método de pagamento', 'Cartão de crédito MasterCard'),
(794, 112, 'Parcelas', '1'),
(795, 112, '_wc_pagseguro_payment_data', 'a:4:{s:4:"type";i:1;s:6:"method";s:30:"Cartão de crédito MasterCard";s:12:"installments";s:1:"1";s:4:"link";s:0:"";}'),
(796, 112, '_recorded_sales', 'yes'),
(797, 112, '_recorded_coupon_usage_counts', 'yes'),
(798, 112, '_edit_lock', '1543066569:1'),
(799, 115, '_wc_review_count', '0'),
(800, 115, '_wc_rating_count', 'a:0:{}'),
(801, 115, '_wc_average_rating', '0'),
(802, 115, '_edit_lock', '1543064648:1'),
(803, 115, '_edit_last', '1'),
(805, 117, '_wc_review_count', '0'),
(806, 117, '_wc_rating_count', 'a:0:{}'),
(807, 117, '_wc_average_rating', '0'),
(808, 117, '_edit_lock', '1543070004:1'),
(812, 119, '_wp_attached_file', '2018/11/garrafa-2-e1543070825639.jpg'),
(813, 119, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:327;s:6:"height";i:379;s:4:"file";s:36:"2018/11/garrafa-2-e1543070825639.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:36:"garrafa-2-e1543070825639-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:36:"garrafa-2-e1543070825639-259x300.jpg";s:5:"width";i:259;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:4:{s:4:"file";s:36:"garrafa-2-e1543070825639-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:36:"garrafa-2-e1543070825639-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:36:"garrafa-2-e1543070825639-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:36:"garrafa-2-e1543070825639-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(814, 120, '_wp_attached_file', '2018/11/garrafa-3.jpg'),
(815, 120, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:233;s:6:"height";i:330;s:4:"file";s:21:"2018/11/garrafa-3.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"garrafa-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"garrafa-3-212x300.jpg";s:5:"width";i:212;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:21:"garrafa-3-233x324.jpg";s:5:"width";i:233;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:21:"garrafa-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:21:"garrafa-3-233x324.jpg";s:5:"width";i:233;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:21:"garrafa-3-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(816, 121, '_wp_attached_file', '2018/11/garrafa.jpg'),
(817, 121, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:429;s:6:"height";i:404;s:4:"file";s:19:"2018/11/garrafa.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"garrafa-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"garrafa-300x283.jpg";s:5:"width";i:300;s:6:"height";i:283;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:19:"garrafa-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:19:"garrafa-416x392.jpg";s:5:"width";i:416;s:6:"height";i:392;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:19:"garrafa-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"garrafa-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"garrafa-416x392.jpg";s:5:"width";i:416;s:6:"height";i:392;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"garrafa-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(819, 122, '_wc_review_count', '0'),
(820, 122, '_wc_rating_count', 'a:0:{}'),
(821, 122, '_wc_average_rating', '0'),
(822, 122, '_edit_lock', '1543069200:1'),
(823, 122, '_edit_last', '1'),
(824, 123, '_wp_attached_file', '2018/11/lunch-box.jpg'),
(825, 123, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:411;s:6:"height";i:307;s:4:"file";s:21:"2018/11/lunch-box.jpg";s:5:"sizes";a:6:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"lunch-box-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"lunch-box-300x224.jpg";s:5:"width";i:300;s:6:"height";i:224;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:21:"lunch-box-324x307.jpg";s:5:"width";i:324;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:21:"lunch-box-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:21:"lunch-box-324x307.jpg";s:5:"width";i:324;s:6:"height";i:307;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:21:"lunch-box-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(826, 124, '_wp_attached_file', '2018/11/marmita.jpg'),
(827, 124, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:430;s:6:"height";i:249;s:4:"file";s:19:"2018/11/marmita.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"marmita-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"marmita-300x174.jpg";s:5:"width";i:300;s:6:"height";i:174;s:9:"mime-type";s:10:"image/jpeg";}s:21:"woocommerce_thumbnail";a:5:{s:4:"file";s:19:"marmita-324x249.jpg";s:5:"width";i:324;s:6:"height";i:249;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:18:"woocommerce_single";a:4:{s:4:"file";s:19:"marmita-416x241.jpg";s:5:"width";i:416;s:6:"height";i:241;s:9:"mime-type";s:10:"image/jpeg";}s:29:"woocommerce_gallery_thumbnail";a:4:{s:4:"file";s:19:"marmita-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:12:"shop_catalog";a:4:{s:4:"file";s:19:"marmita-324x249.jpg";s:5:"width";i:324;s:6:"height";i:249;s:9:"mime-type";s:10:"image/jpeg";}s:11:"shop_single";a:4:{s:4:"file";s:19:"marmita-416x241.jpg";s:5:"width";i:416;s:6:"height";i:241;s:9:"mime-type";s:10:"image/jpeg";}s:14:"shop_thumbnail";a:4:{s:4:"file";s:19:"marmita-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(828, 125, '_wc_review_count', '0'),
(829, 125, '_wc_rating_count', 'a:0:{}'),
(830, 125, '_wc_average_rating', '0'),
(831, 125, '_edit_lock', '1543069313:1'),
(832, 125, '_edit_last', '1'),
(833, 117, '_edit_last', '1'),
(834, 117, '_thumbnail_id', '124'),
(835, 117, '_sku', ''),
(836, 117, '_regular_price', '35.60'),
(837, 117, '_sale_price', ''),
(838, 117, '_sale_price_dates_from', ''),
(839, 117, '_sale_price_dates_to', ''),
(840, 117, 'total_sales', '0'),
(841, 117, '_tax_status', 'taxable'),
(842, 117, '_tax_class', ''),
(843, 117, '_manage_stock', 'no'),
(844, 117, '_backorders', 'no'),
(845, 117, '_sold_individually', 'no'),
(846, 117, '_weight', '0.300'),
(847, 117, '_length', '20'),
(848, 117, '_width', '20'),
(849, 117, '_height', '20'),
(850, 117, '_upsell_ids', 'a:0:{}'),
(851, 117, '_crosssell_ids', 'a:0:{}'),
(852, 117, '_purchase_note', ''),
(853, 117, '_default_attributes', 'a:0:{}'),
(854, 117, '_virtual', 'no'),
(855, 117, '_downloadable', 'no'),
(856, 117, '_product_image_gallery', ''),
(857, 117, '_download_limit', '-1'),
(858, 117, '_download_expiry', '-1'),
(859, 117, '_stock', NULL),
(860, 117, '_stock_status', 'instock'),
(861, 117, '_product_version', '3.4.4'),
(862, 117, '_price', '35.60'),
(863, 117, '_product_attributes', 'a:1:{s:11:"pa_material";a:6:{s:4:"name";s:11:"pa_material";s:5:"value";s:0:"";s:8:"position";i:0;s:10:"is_visible";i:1;s:12:"is_variation";i:0;s:11:"is_taxonomy";i:1;}}'),
(864, 127, '_wc_review_count', '0'),
(865, 127, '_wc_rating_count', 'a:0:{}'),
(866, 127, '_wc_average_rating', '0'),
(867, 127, '_edit_lock', '1543071585:1'),
(868, 127, '_edit_last', '1'),
(869, 127, '_sku', ''),
(870, 127, '_regular_price', '59.00'),
(871, 127, '_sale_price', ''),
(872, 127, '_sale_price_dates_from', ''),
(873, 127, '_sale_price_dates_to', ''),
(874, 127, 'total_sales', '0'),
(875, 127, '_tax_status', 'taxable'),
(876, 127, '_tax_class', ''),
(877, 127, '_manage_stock', 'no'),
(878, 127, '_backorders', 'no'),
(879, 127, '_sold_individually', 'no'),
(880, 127, '_weight', ''),
(881, 127, '_length', ''),
(882, 127, '_width', ''),
(883, 127, '_height', ''),
(884, 127, '_upsell_ids', 'a:1:{i:0;i:117;}'),
(885, 127, '_crosssell_ids', 'a:1:{i:0;i:117;}'),
(886, 127, '_purchase_note', ''),
(887, 127, '_default_attributes', 'a:0:{}'),
(888, 127, '_virtual', 'no'),
(889, 127, '_downloadable', 'no'),
(890, 127, '_product_image_gallery', '120,121,119'),
(891, 127, '_download_limit', '-1'),
(892, 127, '_download_expiry', '-1'),
(893, 127, '_stock', NULL),
(894, 127, '_stock_status', 'instock'),
(895, 127, '_product_version', '3.4.4'),
(896, 127, '_price', '59.00'),
(897, 127, '_thumbnail_id', '121'),
(898, 119, '_wp_attachment_backup_sizes', 'a:7:{s:9:"full-orig";a:3:{s:5:"width";i:327;s:6:"height";i:379;s:4:"file";s:13:"garrafa-2.jpg";}s:14:"thumbnail-orig";a:4:{s:4:"file";s:21:"garrafa-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:11:"medium-orig";a:4:{s:4:"file";s:21:"garrafa-2-259x300.jpg";s:5:"width";i:259;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:26:"woocommerce_thumbnail-orig";a:5:{s:4:"file";s:21:"garrafa-2-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";s:9:"uncropped";b:1;}s:34:"woocommerce_gallery_thumbnail-orig";a:4:{s:4:"file";s:21:"garrafa-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}s:17:"shop_catalog-orig";a:4:{s:4:"file";s:21:"garrafa-2-324x324.jpg";s:5:"width";i:324;s:6:"height";i:324;s:9:"mime-type";s:10:"image/jpeg";}s:19:"shop_thumbnail-orig";a:4:{s:4:"file";s:21:"garrafa-2-100x100.jpg";s:5:"width";i:100;s:6:"height";i:100;s:9:"mime-type";s:10:"image/jpeg";}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-08-15 00:46:05', '2018-08-15 03:46:05', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2018-08-15 00:46:05', '2018-08-15 03:46:05', '', 0, 'http://pollev-com.umbler.net/?p=1', 0, 'post', '', 1),
(3, 1, '2018-08-15 00:46:05', '2018-08-15 03:46:05', '<h2>Quem somos</h2><p>O endereço do nosso site é: http://localhost/wordpress.</p><h2>Quais dados pessoais coletamos e porque</h2><h3>Comentários</h3><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><h3>Mídia</h3><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><h3>Formulários de contato</h3><h3>Cookies</h3><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><h3>Mídia incorporada de outros sites</h3><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><h3>Análises</h3><h2>Com quem partilhamos seus dados</h2><h2>Por quanto tempo mantemos os seus dados</h2><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><h2>Quais os seus direitos sobre seus dados</h2><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><h2>Para onde enviamos seus dados</h2><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><h2>Suas informações de contato</h2><h2>Informações adicionais</h2><h3>Como protegemos seus dados</h3><h3>Quais são nossos procedimentos contra violação de dados</h3><h3>De quais terceiros nós recebemos dados</h3><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3>', 'Política de privacidade', '', 'publish', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2018-09-04 03:39:02', '2018-09-04 06:39:02', '', 0, 'http://pollev-com.umbler.net/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-08-15 00:50:39', '2018-08-15 03:50:39', '', 'Todos', '', 'publish', 'closed', 'closed', '', 'loja', '', '', '2018-08-15 22:54:47', '2018-08-16 01:54:47', '', 0, 'http://pollev-com.umbler.net/loja/', 0, 'page', '', 0),
(6, 1, '2018-08-15 00:50:39', '2018-08-15 03:50:39', '[woocommerce_cart]', 'Carrinho', '', 'publish', 'closed', 'closed', '', 'carrinho', '', '', '2018-08-15 00:50:39', '2018-08-15 03:50:39', '', 0, 'http://pollev-com.umbler.net/carrinho/', 0, 'page', '', 0),
(7, 1, '2018-08-15 00:50:39', '2018-08-15 03:50:39', '[woocommerce_checkout]', 'Finalizar compra', '', 'publish', 'closed', 'closed', '', 'finalizar-compra', '', '', '2018-08-15 00:50:39', '2018-08-15 03:50:39', '', 0, 'http://pollev-com.umbler.net/finalizar-compra/', 0, 'page', '', 0),
(8, 1, '2018-08-15 00:50:39', '2018-08-15 03:50:39', '[woocommerce_my_account]', 'Minha conta', '', 'publish', 'closed', 'closed', '', 'minha-conta', '', '', '2018-08-15 00:50:39', '2018-08-15 03:50:39', '', 0, 'http://pollev-com.umbler.net/minha-conta/', 0, 'page', '', 0),
(9, 1, '2018-08-15 00:59:38', '2018-08-15 03:59:38', 'Teste\r\n\r\n<img class="alignnone wp-image-51 size-medium" src="http://localhost/wordpress/wp-content/uploads/2018/08/bolsa-termica-lancheira-oncinha-chic-300x300.jpg" alt="" width="300" height="300" />', 'Bag academia', '<img class="alignnone wp-image-51 size-medium" src="http://localhost/wordpress/wp-content/uploads/2018/08/bolsa-termica-lancheira-oncinha-chic-300x300.jpg" alt="" width="300" height="300" />', 'publish', 'open', 'closed', '', 'bag', '', '', '2018-08-16 01:14:54', '2018-08-16 04:14:54', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=9', 0, 'product', '', 0),
(11, 1, '2018-08-15 01:10:49', '2018-08-15 04:10:49', 'Teste\r\n\r\n<img class="alignnone size-medium wp-image-10" src="http://localhost/wordpress/wp-content/uploads/2018/08/1517576826-462100062-300x175.jpg" alt="" width="300" height="175" />', 'Bag academia', '<img class="alignnone size-medium wp-image-10" src="http://localhost/wordpress/wp-content/uploads/2018/08/1517576826-462100062-300x175.jpg" alt="" width="300" height="175" />', 'inherit', 'closed', 'closed', '', '9-autosave-v1', '', '', '2018-08-15 01:10:49', '2018-08-15 04:10:49', '', 9, 'http://pollev-com.umbler.net/2018/08/15/9-autosave-v1/', 0, 'revision', '', 0),
(105, 1, '2018-08-30 07:56:46', '2018-08-30 10:56:46', '', 'Banner-3', '', 'inherit', 'open', 'closed', '', 'banner-3', '', '', '2018-08-30 07:56:46', '2018-08-30 10:56:46', '', 16, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/Banner-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(106, 1, '2018-08-30 07:56:51', '2018-08-30 10:56:51', '', 'Banner-4', '', 'inherit', 'open', 'closed', '', 'banner-4-2', '', '', '2018-08-30 07:56:51', '2018-08-30 10:56:51', '', 16, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/Banner-4-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2018-08-30 07:56:31', '2018-08-30 10:56:31', '', 'Banner-1', '', 'inherit', 'open', 'closed', '', 'banner-1', '', '', '2018-08-30 07:56:31', '2018-08-30 10:56:31', '', 16, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/Banner-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2018-08-30 07:56:37', '2018-08-30 10:56:37', '', 'Banner-2', '', 'inherit', 'open', 'closed', '', 'banner-2-2', '', '', '2018-08-30 07:56:37', '2018-08-30 10:56:37', '', 16, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/Banner-2-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2018-08-15 18:40:20', '2018-08-15 21:40:20', '', '<br/><br/><br/>', '', 'publish', 'closed', 'closed', '', 'boas-vindas', '', '', '2018-08-30 07:58:17', '2018-08-30 10:58:17', '', 0, 'http://pollev-com.umbler.net/?page_id=16', 0, 'page', '', 0),
(25, 1, '2018-08-15 19:06:11', '2018-08-15 22:06:11', '', 'Boas-vindas', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-15 19:06:11', '2018-08-15 22:06:11', '', 16, 'http://pollev-com.umbler.net/2018/08/15/16-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2018-08-15 19:06:27', '2018-08-15 22:06:27', '', '', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-15 19:06:27', '2018-08-15 22:06:27', '', 16, 'http://pollev-com.umbler.net/2018/08/15/16-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2018-11-24 12:44:43', '2018-11-24 14:44:43', 'Com trava de segurança\r\n700ml\r\nAlça para transporte\r\nIdeal para atividades físicas, escola ou escritório\r\nLeve e fácil de carregar\r\nComposição: Plástico\r\nTamanho: comp. 7,7 cm larg. 7,7 cm alt. 22 cm', 'Garrafa Squeeze Fitness com filtro', 'Garrafa Squeeze com Capacidade de 700ml.\r\nPossui trava de segurança, alça para transporte. \r\nIdeal para atividades físicas, escola ou escritório. \r\nLeve e fácil de carregar\r\n\r\n\r\n', 'publish', 'open', 'closed', '', 'garrafa-squeeze-fitness-com-filtro', '', '', '2018-11-24 12:53:04', '2018-11-24 14:53:04', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=127', 0, 'product', '', 0),
(19, 1, '2018-08-15 18:34:41', '2018-08-15 21:34:41', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-08-15 18:34:41', '2018-08-15 21:34:41', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/logo.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2018-08-15 19:05:50', '2018-08-15 22:05:50', 'Esta é a sua página inicial, a que a maioria dos visitantes verá quando visitar sua loja pela primeira vez.\r\n\r\nVocê pode mudar esse texto editando a página "Boas-vindas" no menu "Páginas" do seu painel de controle.', 'Boas-vindas', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-15 19:05:50', '2018-08-15 22:05:50', '', 16, 'http://pollev-com.umbler.net/2018/08/15/16-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2018-08-15 18:40:20', '2018-08-15 21:40:20', 'Esta é a sua página inicial, a que a maioria dos visitantes verá quando visitar sua loja pela primeira vez.\r\n\r\nVocê pode mudar esse texto editando a página &quot;Boas-vindas&quot; no menu &quot;Páginas&quot; do seu painel de controle.', 'Boas-vindas', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-15 18:40:20', '2018-08-15 21:40:20', '', 16, 'http://pollev-com.umbler.net/2018/08/15/16-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2018-08-15 19:05:27', '2018-08-15 22:05:27', '', 'Banner-2', '', 'inherit', 'open', 'closed', '', 'banner-2', '', '', '2018-08-15 19:05:27', '2018-08-15 22:05:27', '', 16, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/Banner-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2018-08-15 19:07:01', '2018-08-15 22:07:01', '', 'Pollev', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-15 19:07:01', '2018-08-15 22:07:01', '', 16, 'http://pollev-com.umbler.net/2018/08/15/16-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2018-08-15 19:08:50', '2018-08-15 22:08:50', '', 'Todos', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-15 19:08:50', '2018-08-15 22:08:50', '', 5, 'http://pollev-com.umbler.net/2018/08/15/5-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2018-08-23 01:18:42', '2018-08-23 04:18:42', '<img class="alignnone size-medium wp-image-48" src="http://localhost/wordpress/wp-content/uploads/2018/08/689675-MLA26280173077_112017-O-300x300.jpg" alt="" width="300" height="300" />\n\n&nbsp;\n\nGarrafa Tapaware', 'Garrafa Térmica', 'Garrafa térmica tapaware feminina, ide', 'inherit', 'closed', 'closed', '', '47-autosave-v1', '', '', '2018-08-23 01:18:42', '2018-08-23 04:18:42', '', 47, 'http://pollev-com.umbler.net/2018/08/23/47-autosave-v1/', 0, 'revision', '', 0),
(32, 1, '2018-08-15 22:24:02', '2018-08-16 01:24:02', '', 'BANNER_QUADRADOS2', '', 'inherit', 'open', 'closed', '', 'banner_quadrados2', '', '', '2018-08-15 22:24:02', '2018-08-16 01:24:02', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/BANNER_QUADRADOS2.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2018-08-15 22:26:13', '2018-08-16 01:26:13', '', 'BANNER_QUADRADOS2', '', 'inherit', 'open', 'closed', '', 'banner_quadrados2-2', '', '', '2018-08-15 22:26:13', '2018-08-16 01:26:13', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/BANNER_QUADRADOS2-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(34, 1, '2018-08-15 22:26:16', '2018-08-16 01:26:16', '', 'BANNER_QUADRADOS3', '', 'inherit', 'open', 'closed', '', 'banner_quadrados3', '', '', '2018-08-15 22:26:16', '2018-08-16 01:26:16', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/BANNER_QUADRADOS3.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2018-08-15 22:26:18', '2018-08-16 01:26:18', '', 'BANNER_QUADRADOS4', '', 'inherit', 'open', 'closed', '', 'banner_quadrados4', '', '', '2018-08-15 22:26:18', '2018-08-16 01:26:18', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/BANNER_QUADRADOS4.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 1, '2018-08-15 22:26:36', '2018-08-16 01:26:36', '', 'BANNER_QUADRADOS1', '', 'inherit', 'open', 'closed', '', 'banner_quadrados1', '', '', '2018-08-15 22:26:36', '2018-08-16 01:26:36', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/BANNER_QUADRADOS1.jpg', 0, 'attachment', 'image/jpeg', 0),
(37, 1, '2018-08-15 22:29:10', '2018-08-16 01:29:10', '', 'Banner-4', '', 'inherit', 'open', 'closed', '', 'banner-4', '', '', '2018-08-15 22:29:10', '2018-08-16 01:29:10', '', 0, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/Banner-4.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2018-08-15 23:04:00', '2018-08-16 02:04:00', 'Bolsa preta, tamanho g', 'Bolsa Preta', 'Bolsa de couro de jacaré', 'publish', 'open', 'closed', '', 'bolsa-preta', '', '', '2018-08-15 23:04:00', '2018-08-16 02:04:00', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=40', 0, 'product', '', 0),
(41, 1, '2018-08-15 22:54:16', '2018-08-16 01:54:16', '', '', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-15 22:54:16', '2018-08-16 01:54:16', '', 5, 'http://pollev-com.umbler.net/2018/08/15/5-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2018-08-15 22:54:29', '2018-08-16 01:54:29', '', '', '', 'inherit', 'closed', 'closed', '', '5-autosave-v1', '', '', '2018-08-15 22:54:29', '2018-08-16 01:54:29', '', 5, 'http://pollev-com.umbler.net/2018/08/15/5-autosave-v1/', 0, 'revision', '', 0),
(43, 1, '2018-08-15 22:54:47', '2018-08-16 01:54:47', '', 'Todos', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2018-08-15 22:54:47', '2018-08-16 01:54:47', '', 5, 'http://pollev-com.umbler.net/2018/08/15/5-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2018-08-15 22:57:08', '2018-08-16 01:57:08', '', '<br/><br/><br/>', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-08-15 22:57:08', '2018-08-16 01:57:08', '', 16, 'http://pollev-com.umbler.net/2018/08/15/16-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2018-08-15 22:59:32', '2018-08-16 01:59:32', '', '777703172_Detalhes', '', 'inherit', 'open', 'closed', '', '777703172_detalhes', '', '', '2018-08-15 22:59:32', '2018-08-16 01:59:32', '', 40, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/777703172_Detalhes.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2018-08-16 00:59:56', '2018-08-16 03:59:56', '<img class="alignnone size-medium wp-image-48" src="http://localhost/wordpress/wp-content/uploads/2018/08/689675-MLA26280173077_112017-O-300x300.jpg" alt="" width="300" height="300" />\r\n\r\n&nbsp;\r\n\r\nGarrafa Tapaware', 'Garrafa Térmica', 'Garrafa térmica tapaware feminina, ideal para uso durante a prática de esportes e na academia.', 'publish', 'open', 'closed', '', 'garrafa-termica', '', '', '2018-08-23 01:26:21', '2018-08-23 04:26:21', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=47', 0, 'product', '', 0),
(48, 1, '2018-08-16 00:56:19', '2018-08-16 03:56:19', '', '689675-MLA26280173077_112017-O', '', 'inherit', 'open', 'closed', '', '689675-mla26280173077_112017-o', '', '', '2018-08-16 00:56:19', '2018-08-16 03:56:19', '', 47, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/689675-MLA26280173077_112017-O.jpg', 0, 'attachment', 'image/jpeg', 0),
(49, 1, '2018-08-16 01:11:52', '2018-08-16 04:11:52', 'Bolsa térmica bege claro', 'Bolsa Térmica Bege', '', 'publish', 'open', 'closed', '', 'bolsa-termica-bege', '', '', '2018-08-16 01:11:52', '2018-08-16 04:11:52', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=49', 0, 'product', '', 0),
(50, 1, '2018-08-16 01:08:31', '2018-08-16 04:08:31', '', '17126601_224096344726267_8884516374378971136_n', '', 'inherit', 'open', 'closed', '', '17126601_224096344726267_8884516374378971136_n', '', '', '2018-08-16 01:08:31', '2018-08-16 04:08:31', '', 49, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/17126601_224096344726267_8884516374378971136_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2018-08-16 01:13:44', '2018-08-16 04:13:44', '', 'bolsa-termica-lancheira-oncinha-chic', '', 'inherit', 'open', 'closed', '', 'bolsa-termica-lancheira-oncinha-chic', '', '', '2018-08-16 01:13:44', '2018-08-16 04:13:44', '', 9, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/bolsa-termica-lancheira-oncinha-chic.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2018-08-25 12:08:48', '2018-08-25 15:08:48', '', 'Order &ndash; agosto 25, 2018 @ 12:08 PM', '', 'wc-completed', 'closed', 'closed', 'order_5b817100ab79f', 'pedido-25-de-aug-de-2018-as-150808', '', '', '2018-08-25 12:29:25', '2018-08-25 15:29:25', '', 0, 'http://pollev-com.umbler.net/?post_type=shop_order&#038;p=84', 0, 'shop_order', '', 2),
(53, 1, '2018-08-18 17:44:36', '0000-00-00 00:00:00', '', 'teste', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-08-18 17:44:36', '0000-00-00 00:00:00', '', 0, 'http://pollev-com.umbler.net/?p=53', 1, 'nav_menu_item', '', 0),
(115, 1, '2018-11-24 10:49:57', '0000-00-00 00:00:00', '<img class="alignnone size-medium wp-image-50" src="http://pollev-com.umbler.net/wp-content/uploads/2018/08/17126601_224096344726267_8884516374378971136_n-300x300.jpg" alt="" width="300" height="300" />', 'Bolsa Termica Art G', '', 'draft', 'open', 'closed', '', '', '', '', '2018-11-24 10:49:57', '2018-11-24 12:49:57', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=115', 0, 'product', '', 0),
(117, 1, '2018-11-24 12:30:43', '2018-11-24 14:30:43', '&nbsp;\r\n\r\nINSTRUÇÕES DE USO: Retire a tampa antes de levar ao microondas;\r\n\r\nusar somente para aquecer comida, não para cozinhar;\r\n\r\nVai ao freezer e lava-louças; Temperatura de uso: -20°C ~120°C.)\r\n<ul>\r\n 	<li>Comporta até 1400 ml</li>\r\n 	<li>Inclui pote para molho</li>\r\n 	<li>Livre de BPA</li>\r\n 	<li>Vedação com fita de silicone</li>\r\n 	<li>Vai ao microondas (retire a tampa)</li>\r\n 	<li>Temperatura de uso: -20°C ~120°C</li>\r\n</ul>\r\nComposição: Polipropileno\r\n\r\nTamanho: Comp. 21cm  Larg. 15,2cm  Alt. 8,3cm\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Marmita com porta Molho', '<strong>Para quem deseja sempre levar sua refeição preparada com carinho para outros lugares, a marmita com três compartimentos possui um pote para molho e espaço amplo interno e divisórias.\r\n\r\n<strong>Produto livre de BPA</strong>', 'publish', 'open', 'closed', '', 'marmita-com-porta-molho', '', '', '2018-11-24 12:35:38', '2018-11-24 14:35:38', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=117', 0, 'product', '', 0),
(119, 1, '2018-11-24 12:18:16', '2018-11-24 14:18:16', '', 'Garrafa 700ml', '', 'inherit', 'open', 'closed', '', 'garrafa-2', '', '', '2018-11-24 12:36:28', '2018-11-24 14:36:28', '', 117, 'http://pollev-com.umbler.net/wp-content/uploads/2018/11/garrafa-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(120, 1, '2018-11-24 12:18:17', '2018-11-24 14:18:17', '', 'garrafa 3', '', 'inherit', 'open', 'closed', '', 'garrafa-3', '', '', '2018-11-24 12:18:17', '2018-11-24 14:18:17', '', 117, 'http://pollev-com.umbler.net/wp-content/uploads/2018/11/garrafa-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(121, 1, '2018-11-24 12:18:19', '2018-11-24 14:18:19', '', 'garrafa', '', 'inherit', 'open', 'closed', '', 'garrafa', '', '', '2018-11-24 12:18:19', '2018-11-24 14:18:19', '', 117, 'http://pollev-com.umbler.net/wp-content/uploads/2018/11/garrafa.jpg', 0, 'attachment', 'image/jpeg', 0),
(122, 1, '2018-11-24 12:20:00', '0000-00-00 00:00:00', '', '', '<p><br data-mce-bogus="1"></p>', 'draft', 'open', 'closed', '', '', '', '', '2018-11-24 12:20:00', '2018-11-24 14:20:00', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=122', 0, 'product', '', 0),
(123, 1, '2018-11-24 12:20:26', '2018-11-24 14:20:26', '', 'lunch box', '', 'inherit', 'open', 'closed', '', 'lunch-box', '', '', '2018-11-24 12:20:26', '2018-11-24 14:20:26', '', 117, 'http://pollev-com.umbler.net/wp-content/uploads/2018/11/lunch-box.jpg', 0, 'attachment', 'image/jpeg', 0),
(124, 1, '2018-11-24 12:20:28', '2018-11-24 14:20:28', 'Descrição: Para quem deseja sempre levar sua refeição preparada com carinho para outros lugares, a marmita com três compartimentos possui um pote para molho e espaço amplo interno e divisórias. Produto livre de BPA com qualidade Jacki Design. (INSTRUÇÕES DE USO: Retire a tampa antes de levar ao microondas; usar somente para aquecer comida, não para cozinhar; Vai ao freezer e lava-louças; Temperatura de uso: -20°C ~120°C.)\n• Comporta até 1400 ml  \n• Inclui pote para molho            \n• Livre de BPA                                        \n• Vedação com fita de silicone                      \n• Vai ao microondas (retire a tampa)      \n• Temperatura de uso: -20°C ~120°C\nComposição: Polipropileno\nTamanho: Comp. 21cm  Larg. 15,2cm  Alt. 8,3cm\n', 'marmita', 'Marmita com Porta Molho', 'inherit', 'open', 'closed', '', 'marmita', '', '', '2018-11-24 12:21:56', '2018-11-24 14:21:56', '', 117, 'http://pollev-com.umbler.net/wp-content/uploads/2018/11/marmita.jpg', 0, 'attachment', 'image/jpeg', 0),
(125, 1, '2018-11-24 12:21:53', '0000-00-00 00:00:00', '', 'Garrafa', '<p><br data-mce-bogus="1"></p>', 'draft', 'open', 'closed', '', '', '', '', '2018-11-24 12:21:53', '2018-11-24 14:21:53', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=125', 0, 'product', '', 0),
(126, 1, '2018-11-24 12:34:42', '2018-11-24 14:34:42', '&nbsp;\r\n\r\nINSTRUÇÕES DE USO: Retire a tampa antes de levar ao microondas;\r\n\r\nusar somente para aquecer comida, não para cozinhar;\r\n\r\nVai ao freezer e lava-louças; Temperatura de uso: -20°C ~120°C.)\r\n<ul>\r\n 	<li>Comporta até 1400 ml</li>\r\n 	<li>Inclui pote para molho</li>\r\n 	<li>Livre de BPA</li>\r\n 	<li>Vedação com fita de silicone</li>\r\n 	<li>Vai ao microondas (retire a tampa)</li>\r\n 	<li>Temperatura de uso: -20°C ~120°C</li>\r\n</ul>\r\nComposição: Polipropileno\r\n\r\nTamanho: Comp. 21cm  Larg. 15,2cm  Alt. 8,3cm\r\n\r\n&nbsp;\r\n\r\n&nbsp;', 'Marmita com porta Molho', '<strong>Para quem deseja sempre levar sua refeição preparada com carinho para outros lugares, a marmita com três compartimentos possui um pote para molho e espaço amplo interno e divisórias.\r\n\r\n<strong>Produto livre de BPA</strong>', 'inherit', 'closed', 'closed', '', '117-autosave-v1', '', '', '2018-11-24 12:34:42', '2018-11-24 14:34:42', '', 117, 'http://pollev-com.umbler.net/2018/11/24/117-autosave-v1/', 0, 'revision', '', 0),
(55, 1, '2018-08-18 17:52:54', '2018-08-18 20:52:54', ' ', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2018-08-23 01:28:41', '2018-08-23 04:28:41', '', 0, 'http://pollev-com.umbler.net/2018/08/18/55/', 1, 'nav_menu_item', '', 0),
(56, 1, '2018-08-18 17:52:54', '2018-08-18 20:52:54', ' ', '', '', 'publish', 'closed', 'closed', '', '56', '', '', '2018-08-23 01:28:41', '2018-08-23 04:28:41', '', 0, 'http://pollev-com.umbler.net/2018/08/18/56/', 4, 'nav_menu_item', '', 0),
(57, 1, '2018-08-18 17:52:54', '2018-08-18 20:52:54', ' ', '', '', 'publish', 'closed', 'closed', '', '57', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 0, 'http://pollev-com.umbler.net/2018/08/18/57/', 7, 'nav_menu_item', '', 0),
(58, 1, '2018-08-18 17:52:54', '2018-08-18 20:52:54', ' ', '', '', 'publish', 'closed', 'closed', '', '58', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 0, 'http://pollev-com.umbler.net/2018/08/18/58/', 10, 'nav_menu_item', '', 0),
(59, 1, '2018-08-18 17:52:54', '2018-08-18 20:52:54', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 0, 'http://pollev-com.umbler.net/2018/08/18/59/', 13, 'nav_menu_item', '', 0),
(62, 1, '2018-08-18 18:21:29', '2018-08-18 21:21:29', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2018-08-23 01:28:41', '2018-08-23 04:28:41', '', 16, 'http://pollev-com.umbler.net/?p=62', 2, 'nav_menu_item', '', 0),
(63, 1, '2018-08-18 18:21:29', '2018-08-18 21:21:29', ' ', '', '', 'publish', 'closed', 'closed', '', '63', '', '', '2018-08-23 01:28:41', '2018-08-23 04:28:41', '', 16, 'http://pollev-com.umbler.net/?p=63', 3, 'nav_menu_item', '', 0),
(92, 1, '2018-08-29 09:49:58', '2018-08-29 12:49:58', '<div id="accordionIntitucional" class="accordion">\r\n<div class="card">\r\n<div id="headingOne" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link" type="button" data-toggle="collapse" data-target="#geral" aria-expanded="true" aria-controls="geral">\r\nInformações Gerais\r\n</button></h5>\r\n</div>\r\n<div id="geral" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingTwo" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#pagamentos" aria-expanded="false" aria-controls="pagamentos">\r\nFormas de Pagamento\r\n</button></h5>\r\n</div>\r\n<div id="pagamentos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#novidades" aria-expanded="false" aria-controls="novidades">\r\nInformativa e-mail e novidades\r\n</button></h5>\r\n</div>\r\n<div id="novidades" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#politica" aria-expanded="false" aria-controls="politica">\r\nPolítica de privacidade\r\n</button></h5>\r\n</div>\r\n<div id="politica" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#conta" aria-expanded="false" aria-controls="conta">\r\nSua Conta\r\n</button></h5>\r\n</div>\r\n<div id="conta" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#trocas" aria-expanded="false" aria-controls="trocas">\r\nTrocas e Devoluções\r\n</button></h5>\r\n</div>\r\n<div id="trocas" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n</div>', 'Institucional', '', 'publish', 'closed', 'closed', '', 'institucional', '', '', '2018-08-29 09:54:17', '2018-08-29 12:54:17', '', 0, 'http://pollev-com.umbler.net/?page_id=92', 0, 'page', '', 0),
(65, 1, '2018-08-20 00:04:41', '2018-08-20 03:04:41', '', '<br/><br/><br/>', '', 'inherit', 'closed', 'closed', '', '16-autosave-v1', '', '', '2018-08-20 00:04:41', '2018-08-20 03:04:41', '', 16, 'http://pollev-com.umbler.net/2018/08/20/16-autosave-v1/', 0, 'revision', '', 0),
(66, 1, '2018-08-20 00:05:02', '2018-08-20 03:05:02', '[woocommerce_cart]', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '6-autosave-v1', '', '', '2018-08-20 00:05:02', '2018-08-20 03:05:02', '', 6, 'http://pollev-com.umbler.net/2018/08/20/6-autosave-v1/', 0, 'revision', '', 0),
(112, 1, '2018-10-17 01:20:29', '2018-10-17 04:20:29', '', 'Order &ndash; outubro 17, 2018 @ 01:20 AM', '', 'wc-on-hold', 'open', 'closed', 'order_5bc6b88d35fca', 'pedido-17-de-oct-de-2018-as-042020', '', '', '2018-10-17 01:20:34', '2018-10-17 04:20:34', '', 0, 'http://pollev-com.umbler.net/?post_type=shop_order&#038;p=112', 0, 'shop_order', '', 1),
(109, 1, '2018-10-08 20:45:56', '2018-10-08 23:45:56', '', 'Order &ndash; outubro 8, 2018 @ 08:45 PM', '', 'wc-on-hold', 'open', 'closed', 'order_5bbbec341e011', 'pedido-08-de-oct-de-2018-as-234545', '', '', '2018-10-08 20:46:00', '2018-10-08 23:46:00', '', 0, 'http://pollev-com.umbler.net/?post_type=shop_order&#038;p=109', 0, 'shop_order', '', 1),
(67, 1, '2018-08-21 20:25:38', '2018-08-21 23:25:38', '', 'Order &ndash; agosto 21, 2018 @ 08:25 PM', '', 'wc-cancelled', 'open', 'closed', 'order_5b7c9f72ba8c1', 'pedido-21-de-aug-de-2018-as-232525', '', '', '2018-08-22 10:04:38', '2018-08-22 13:04:38', '', 0, 'http://pollev-com.umbler.net/?post_type=shop_order&#038;p=67', 0, 'shop_order', '', 1),
(70, 1, '2018-08-22 23:48:39', '2018-08-23 02:48:39', '', 'Order &ndash; agosto 22, 2018 @ 11:48 PM', '', 'wc-cancelled', 'open', 'closed', 'order_5b7e208756a89', 'pedido-23-de-aug-de-2018-as-024848', '', '', '2018-08-23 00:49:16', '2018-08-23 03:49:16', '', 0, 'http://pollev-com.umbler.net/?post_type=shop_order&#038;p=70', 0, 'shop_order', '', 1),
(73, 1, '2018-08-23 01:28:41', '2018-08-23 04:28:41', ' ', '', '', 'publish', 'closed', 'closed', '', '73', '', '', '2018-08-23 01:28:41', '2018-08-23 04:28:41', '', 16, 'http://pollev-com.umbler.net/?p=73', 5, 'nav_menu_item', '', 0),
(74, 1, '2018-08-23 01:28:42', '2018-08-23 04:28:42', ' ', '', '', 'publish', 'closed', 'closed', '', '74', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 16, 'http://pollev-com.umbler.net/?p=74', 6, 'nav_menu_item', '', 0),
(75, 1, '2018-08-23 01:28:42', '2018-08-23 04:28:42', ' ', '', '', 'publish', 'closed', 'closed', '', '75', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 16, 'http://pollev-com.umbler.net/?p=75', 8, 'nav_menu_item', '', 0),
(76, 1, '2018-08-23 01:28:42', '2018-08-23 04:28:42', ' ', '', '', 'publish', 'closed', 'closed', '', '76', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 16, 'http://pollev-com.umbler.net/?p=76', 9, 'nav_menu_item', '', 0),
(77, 1, '2018-08-23 01:28:42', '2018-08-23 04:28:42', ' ', '', '', 'publish', 'closed', 'closed', '', '77', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 16, 'http://pollev-com.umbler.net/?p=77', 11, 'nav_menu_item', '', 0),
(78, 1, '2018-08-23 01:28:42', '2018-08-23 04:28:42', ' ', '', '', 'publish', 'closed', 'closed', '', '78', '', '', '2018-08-23 01:28:42', '2018-08-23 04:28:42', '', 16, 'http://pollev-com.umbler.net/?p=78', 12, 'nav_menu_item', '', 0),
(79, 1, '2018-08-23 01:28:43', '2018-08-23 04:28:43', ' ', '', '', 'publish', 'closed', 'closed', '', '79', '', '', '2018-08-23 01:28:43', '2018-08-23 04:28:43', '', 16, 'http://pollev-com.umbler.net/?p=79', 14, 'nav_menu_item', '', 0),
(80, 1, '2018-08-23 01:28:43', '2018-08-23 04:28:43', ' ', '', '', 'publish', 'closed', 'closed', '', '80', '', '', '2018-08-23 01:28:43', '2018-08-23 04:28:43', '', 16, 'http://pollev-com.umbler.net/?p=80', 15, 'nav_menu_item', '', 0),
(107, 1, '2018-09-04 03:39:02', '2018-09-04 06:39:02', '<h2>Quem somos</h2><p>O endereço do nosso site é: http://localhost/wordpress.</p><h2>Quais dados pessoais coletamos e porque</h2><h3>Comentários</h3><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><h3>Mídia</h3><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><h3>Formulários de contato</h3><h3>Cookies</h3><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><h3>Mídia incorporada de outros sites</h3><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><h3>Análises</h3><h2>Com quem partilhamos seus dados</h2><h2>Por quanto tempo mantemos os seus dados</h2><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><h2>Quais os seus direitos sobre seus dados</h2><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><h2>Para onde enviamos seus dados</h2><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><h2>Suas informações de contato</h2><h2>Informações adicionais</h2><h3>Como protegemos seus dados</h3><h3>Quais são nossos procedimentos contra violação de dados</h3><h3>De quais terceiros nós recebemos dados</h3><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3>', 'Política de privacidade', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-09-04 03:39:02', '2018-09-04 06:39:02', '', 3, 'http://pollev-com.umbler.net/2018/09/04/3-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2018-08-23 18:05:13', '2018-08-23 21:05:13', '', 'Order &ndash; agosto 23, 2018 @ 06:05 PM', '', 'wc-on-hold', 'open', 'closed', 'order_5b7f218944b75', 'pedido-23-de-aug-de-2018-as-210505', '', '', '2018-08-23 18:05:26', '2018-08-23 21:05:26', '', 0, 'http://pollev-com.umbler.net/?post_type=shop_order&#038;p=82', 0, 'shop_order', '', 1),
(83, 1, '2018-08-23 18:13:17', '0000-00-00 00:00:00', '', 'Produto Teste', '', 'draft', 'open', 'closed', '', '', '', '', '2018-08-23 18:13:17', '2018-08-23 21:13:17', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=83', 0, 'product', '', 0),
(85, 1, '2018-08-25 12:58:02', '2018-08-25 15:58:02', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget facilisis dui. Maecenas dapibus, velit at vulputate vulputate, urna mi tempus sem, eget congue nulla nisi a enim. Etiam efficitur nunc at risus laoreet ornare. In sagittis dignissim luctus. Proin iaculis eu justo sit amet ornare. Etiam convallis commodo ligula, a pretium dui placerat vel. Mauris vel lectus non eros finibus scelerisque. Nunc in hendrerit augue, sollicitudin tempus massa.', 'Garrafa Térmica de Alumínio', 'Proin iaculis eu justo sit amet ornare. Etiam convallis commodo ligula, a pretium dui placerat vel. Mauris vel lectus non eros finibus scelerisque. Nunc in hendrerit augue, sollicitudin tempus massa.', 'publish', 'open', 'closed', '', 'garrafa-termica-de-aluminio', '', '', '2018-08-25 13:09:34', '2018-08-25 16:09:34', '', 0, 'http://pollev-com.umbler.net/?post_type=product&#038;p=85', 0, 'product', '', 0),
(86, 1, '2018-08-25 12:42:32', '2018-08-25 15:42:32', '', 'images', '', 'inherit', 'open', 'closed', '', 'images', '', '', '2018-08-25 12:42:32', '2018-08-25 15:42:32', '', 85, 'http://pollev-com.umbler.net/wp-content/uploads/2018/08/images.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2018-08-25 13:09:15', '2018-08-25 16:09:15', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget facilisis dui. Maecenas dapibus, velit at vulputate vulputate, urna mi tempus sem, eget congue nulla nisi a enim. Etiam efficitur nunc at risus laoreet ornare. In sagittis dignissim luctus. Proin iaculis eu justo sit amet ornare. Etiam convallis commodo ligula, a pretium dui placerat vel. Mauris vel lectus non eros finibus scelerisque. Nunc in hendrerit augue, sollicitudin tempus massa.', 'Garrafa Térmica de Alumínio', 'Proin iaculis eu justo sit amet ornare. Etiam convallis commodo ligula, a pretium dui placerat vel. Mauris vel lectus non eros finibus scelerisque. Nunc in hendrerit augue, sollicitudin tempus massa.', 'inherit', 'closed', 'closed', '', '85-autosave-v1', '', '', '2018-08-25 13:09:15', '2018-08-25 16:09:15', '', 85, 'http://pollev-com.umbler.net/2018/08/25/85-autosave-v1/', 0, 'revision', '', 0),
(89, 1, '2018-08-25 13:08:26', '2018-08-25 16:08:26', 'Bolsa térmica bege claro', 'Bolsa Térmica Bege', '<p><br data-mce-bogus="1"></p>', 'inherit', 'closed', 'closed', '', '49-autosave-v1', '', '', '2018-08-25 13:08:26', '2018-08-25 16:08:26', '', 49, 'http://pollev-com.umbler.net/2018/08/25/49-autosave-v1/', 0, 'revision', '', 0),
(93, 1, '2018-08-28 11:09:59', '2018-08-28 14:09:59', '<div id="accordionExample" class="accordion">\r\n<div class="card">\r\n<div id="headingOne" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">\r\nCollapsible Group Item #1\r\n</button></h5>\r\n</div>\r\n<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n<div class="card">\r\n<div id="headingTwo" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">\r\nCollapsible Group Item #2\r\n</button></h5>\r\n</div>\r\n<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">\r\nCollapsible Group Item #3\r\n</button></h5>\r\n</div>\r\n<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n</div>', 'Intitucional', '', 'inherit', 'closed', 'closed', '', '92-revision-v1', '', '', '2018-08-28 11:09:59', '2018-08-28 14:09:59', '', 92, 'http://pollev-com.umbler.net/2018/08/28/92-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(94, 1, '2018-08-29 09:49:56', '2018-08-29 12:49:56', '<div id="accordionIntitucional" class="accordion">\r\n<div class="card">\r\n<div id="headingOne" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link" type="button" data-toggle="collapse" data-target="#geral" aria-expanded="true" aria-controls="geral">\r\nInformações Gerais\r\n</button></h5>\r\n</div>\r\n<div id="geral" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingTwo" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#pagamentos" aria-expanded="false" aria-controls="pagamentos">\r\nFormas de Pagamento\r\n</button></h5>\r\n</div>\r\n<div id="pagamentos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#novidades" aria-expanded="false" aria-controls="novidades">\r\nInformativa e-mail e novidades\r\n</button></h5>\r\n</div>\r\n<div id="novidades" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#politica" aria-expanded="false" aria-controls="politica">\r\nPolítica de privacidade\r\n</button></h5>\r\n</div>\r\n<div id="politica" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#conta" aria-expanded="false" aria-controls="conta">\r\nSua Conta\r\n</button></h5>\r\n</div>\r\n<div id="conta" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n<div class="card">\r\n<div id="headingThree" class="card-header">\r\n<h5 class="mb-0"><button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#trocas" aria-expanded="false" aria-controls="trocas">\r\nTrocas e Devoluções\r\n</button></h5>\r\n</div>\r\n<div id="trocas" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIntitucional">\r\n<div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven''t heard of them accusamus labore sustainable VHS.</div>\r\n</div>\r\n</div>\r\n\r\n</div>', 'Institucional', '', 'inherit', 'closed', 'closed', '', '92-revision-v1', '', '', '2018-08-29 09:49:56', '2018-08-29 12:49:56', '', 92, 'http://pollev-com.umbler.net/2018/08/29/92-revision-v1/', 0, 'revision', '', 0),
(95, 1, '2018-08-29 10:30:59', '2018-08-29 13:30:59', '<div id="carouselRevendedoras" class="carousel slide" data-ride="carousel">\r\n        <div class="carousel-inner">\r\n          <div class="carousel-item active">\r\n             <div class="container">\r\n                 <h3 class="estado">BAHIA</h3>\r\n                 <br>\r\n                 <div class="col-md-10 offset-1">\r\n                     <div class="row">\r\n                         <div class="col-md-4">\r\n                             <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                             </div>\r\n                         </div>\r\n                         <div class="col-md-4">\r\n                            <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="col-md-4">\r\n                            <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                     </div>\r\n                 </div>\r\n             </div>     \r\n          </div>\r\n          <div class="carousel-item">\r\n             <div class="container">\r\n                <h3 class="estado">SERGIPE</h3>\r\n                <br>\r\n                <div class="col-md-10 offset-1">\r\n                        <div class="row">\r\n                            <div class="col-md-4">\r\n                                <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class="col-md-4">\r\n                               <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                               </div>\r\n                           </div>\r\n                           <div class="col-md-4">\r\n                               <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                               </div>\r\n                           </div>\r\n                        </div>\r\n                    </div>\r\n             </div> \r\n          </div>\r\n          <div class="carousel-item">\r\n            <div class="container">\r\n              <h3 class="estado">CEARÁ</h3>\r\n              <br>\r\n              <div class="col-md-10 offset-1">\r\n                    <div class="row">\r\n                        <div class="col-md-4">\r\n                            <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="col-md-4">\r\n                           <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                           </div>\r\n                       </div>\r\n                       <div class="col-md-4">\r\n                           <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                           </div>\r\n                       </div>\r\n                    </div>\r\n                </div>\r\n            </div> \r\n        </div>\r\n        <a class="carousel-control-prev" href="#carouselRevendedoras" role="button" data-slide="prev">\r\n          <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-left.png" alt="Previous">\r\n          <span class="sr-only">Previous</span>\r\n        </a>\r\n        <a class="carousel-control-next" href="#carouselRevendedoras" role="button" data-slide="next">\r\n          <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="Next">\r\n          <span class="sr-only">Next</span>\r\n        </a>\r\n      </div>', 'Consultoras', '', 'publish', 'closed', 'closed', '', 'consultoras', '', '', '2018-08-29 10:34:33', '2018-08-29 13:34:33', '', 0, 'http://pollev-com.umbler.net/?page_id=95', 0, 'page', '', 0),
(96, 1, '2018-08-29 10:30:47', '2018-08-29 13:30:47', '<div id="carouselRevendedoras" class="carousel slide" data-ride="carousel">\r\n        <div class="carousel-inner">\r\n          <div class="carousel-item active">\r\n             <div class="container">\r\n                 <h3 class="estado">BAHIA</h3>\r\n                 <br>\r\n                 <div class="col-md-10 offset-1">\r\n                     <div class="row">\r\n                         <div class="col-md-4">\r\n                             <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                             </div>\r\n                         </div>\r\n                         <div class="col-md-4">\r\n                            <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="col-md-4">\r\n                            <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                     </div>\r\n                 </div>\r\n             </div>     \r\n          </div>\r\n          <div class="carousel-item">\r\n             <div class="container">\r\n                <h3 class="estado">SERGIPE</h3>\r\n                <br>\r\n                <div class="col-md-10 offset-1">\r\n                        <div class="row">\r\n                            <div class="col-md-4">\r\n                                <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class="col-md-4">\r\n                               <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                               </div>\r\n                           </div>\r\n                           <div class="col-md-4">\r\n                               <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                               </div>\r\n                           </div>\r\n                        </div>\r\n                    </div>\r\n             </div> \r\n          </div>\r\n          <div class="carousel-item">\r\n            <div class="container">\r\n              <h3 class="estado">CEARÁ</h3>\r\n              <br>\r\n              <div class="col-md-10 offset-1">\r\n                    <div class="row">\r\n                        <div class="col-md-4">\r\n                            <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="col-md-4">\r\n                           <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                           </div>\r\n                       </div>\r\n                       <div class="col-md-4">\r\n                           <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                           </div>\r\n                       </div>\r\n                    </div>\r\n                </div>\r\n            </div> \r\n        </div>\r\n        <a class="carousel-control-prev" href="#carouselRevendedoras" role="button" data-slide="prev">\r\n          <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-left.png" alt="Previous">\r\n          <span class="sr-only">Previous</span>\r\n        </a>\r\n        <a class="carousel-control-next" href="#carouselRevendedoras" role="button" data-slide="next">\r\n          <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="Next">\r\n          <span class="sr-only">Next</span>\r\n        </a>\r\n      </div>', 'Consultoras', '', 'inherit', 'closed', 'closed', '', '95-revision-v1', '', '', '2018-08-29 10:30:47', '2018-08-29 13:30:47', '', 95, 'http://pollev-com.umbler.net/2018/08/29/95-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2018-08-29 10:34:22', '2018-08-29 13:34:22', '<div id="carouselRevendedoras" class="carousel slide" data-ride="carousel">\r\n        <div class="carousel-inner">\r\n          <div class="carousel-item active">\r\n             <div class="container">\r\n                 <h3 class="estado">BAHIA</h3>\r\n                 <br>\r\n                 <div class="col-md-10 offset-1">\r\n                     <div class="row">\r\n                         <div class="col-md-4">\r\n                             <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                             </div>\r\n                         </div>\r\n                         <div class="col-md-4">\r\n                            <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="col-md-4">\r\n                            <div class="row">\r\n                                <div class="col-md-4">\r\n                                    <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                </div>\r\n                                <div class="col-md-8">\r\n                                    <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                    <h6>Bairro - Canela</h6>\r\n                                    <h6>ritadecassia@gamil.com</h6>\r\n                                    <h6>(71) 0000-0000</h6>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                     </div>\r\n                 </div>\r\n             </div>     \r\n          </div>\r\n          <div class="carousel-item">\r\n             <div class="container">\r\n                <h3 class="estado">SERGIPE</h3>\r\n                <br>\r\n                <div class="col-md-10 offset-1">\r\n                        <div class="row">\r\n                            <div class="col-md-4">\r\n                                <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class="col-md-4">\r\n                               <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                               </div>\r\n                           </div>\r\n                           <div class="col-md-4">\r\n                               <div class="row">\r\n                                   <div class="col-md-4">\r\n                                       <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                                   </div>\r\n                                   <div class="col-md-8">\r\n                                       <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                       <h6>Bairro - Canela</h6>\r\n                                       <h6>ritadecassia@gamil.com</h6>\r\n                                       <h6>(71) 0000-0000</h6>\r\n                                   </div>\r\n                               </div>\r\n                           </div>\r\n                        </div>\r\n                    </div>\r\n             </div> \r\n          </div>\r\n          <div class="carousel-item">\r\n            <div class="container">\r\n              <h3 class="estado">CEARÁ</h3>\r\n              <br>\r\n              <div class="col-md-10 offset-1">\r\n                    <div class="row">\r\n                        <div class="col-md-4">\r\n                            <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class="col-md-4">\r\n                           <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                           </div>\r\n                       </div>\r\n                       <div class="col-md-4">\r\n                           <div class="row">\r\n                               <div class="col-md-4">\r\n                                   <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/avatar.png" width="100%" class="rounded-circle" alt="">\r\n                               </div>\r\n                               <div class="col-md-8">\r\n                                   <h5>Rita de Cassia <br/> Salvador, Ba</h5>\r\n                                   <h6>Bairro - Canela</h6>\r\n                                   <h6>ritadecassia@gamil.com</h6>\r\n                                   <h6>(71) 0000-0000</h6>\r\n                               </div>\r\n                           </div>\r\n                       </div>\r\n                    </div>\r\n                </div>\r\n            </div> \r\n        </div>\r\n        <a class="carousel-control-prev" href="#carouselRevendedoras" role="button" data-slide="prev">\r\n          <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-left.png" alt="Previous">\r\n          <span class="sr-only">Previous</span>\r\n        </a>\r\n        <a class="carousel-control-next" href="#carouselRevendedoras" role="button" data-slide="next">\r\n          <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="Next">\r\n          <span class="sr-only">Next</span>\r\n        </a>\r\n      </div>', 'Consultoras', '', 'inherit', 'closed', 'closed', '', '95-autosave-v1', '', '', '2018-08-29 10:34:22', '2018-08-29 13:34:22', '', 95, 'http://pollev-com.umbler.net/2018/08/29/95-autosave-v1/', 0, 'revision', '', 0),
(98, 1, '2018-08-29 23:26:04', '2018-08-30 02:26:04', '<div class="contato">\r\n    <div class="container">\r\n        <br />\r\n        <div class="row">\r\n            <div class="col-md-7">\r\n                <div class="form-group row">\r\n                    <label for="nome" class="col-sm-3 col-form-label">Nome <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <input type="text" class="form-control input-sm" id="nome" placeholder="Nome">\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="email" class="col-sm-3 col-form-label">Email <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <input type="email" class="form-control input-sm" id="email" placeholder="exemplo@exemplo">\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="tel" class="col-sm-3 col-form-label">Telefone <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <input type="tel" class="form-control input-sm" id="tel" placeholder="00 0000-0000">\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="msg" class="col-sm-3 col-form-label">Mensagem <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <textarea name="msg" id="msg" class="form-control input-sm" rows="10" placeholder="Sua Mensagem"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="msg" class="col-sm-10 col-form-label"></label>\r\n                    <div class="col-sm-2">\r\n                        <button type="button" class="btn btn-button btn-success">Enviar</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>', 'Fale com a gente', '', 'publish', 'closed', 'closed', '', 'atendimento', '', '', '2018-08-29 23:26:04', '2018-08-30 02:26:04', '', 0, 'http://pollev-com.umbler.net/?page_id=98', 0, 'page', '', 0),
(99, 1, '2018-08-29 23:26:04', '2018-08-30 02:26:04', '<div class="contato">\r\n    <div class="container">\r\n        <br />\r\n        <div class="row">\r\n            <div class="col-md-7">\r\n                <div class="form-group row">\r\n                    <label for="nome" class="col-sm-3 col-form-label">Nome <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <input type="text" class="form-control input-sm" id="nome" placeholder="Nome">\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="email" class="col-sm-3 col-form-label">Email <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <input type="email" class="form-control input-sm" id="email" placeholder="exemplo@exemplo">\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="tel" class="col-sm-3 col-form-label">Telefone <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <input type="tel" class="form-control input-sm" id="tel" placeholder="00 0000-0000">\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="msg" class="col-sm-3 col-form-label">Mensagem <span class="requeride">*</span></label>\r\n                    <div class="col-sm-9">\r\n                        <textarea name="msg" id="msg" class="form-control input-sm" rows="10" placeholder="Sua Mensagem"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class="form-group row">\r\n                    <label for="msg" class="col-sm-10 col-form-label"></label>\r\n                    <div class="col-sm-2">\r\n                        <button type="button" class="btn btn-button btn-success">Enviar</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>', 'Fale com a gente', '', 'inherit', 'closed', 'closed', '', '98-revision-v1', '', '', '2018-08-29 23:26:04', '2018-08-30 02:26:04', '', 98, 'http://pollev-com.umbler.net/2018/08/29/98-revision-v1/', 0, 'revision', '', 0),
(100, 1, '2018-08-29 23:29:39', '2018-08-30 02:29:39', '<div class="lojas">\r\n    <div class="container">\r\n        <br>\r\n        <div class="row">\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>', 'Nossas Lojas | <span style="color: #e8e223; text-transform: none; letter-spacing: 0px;">Encontre a Pollev mais perto de você.</span>', '', 'publish', 'closed', 'closed', '', 'nossas-lojas', '', '', '2018-08-29 23:29:39', '2018-08-30 02:29:39', '', 0, 'http://pollev-com.umbler.net/?page_id=100', 0, 'page', '', 0),
(101, 1, '2018-08-29 23:29:39', '2018-08-30 02:29:39', '<div class="lojas">\r\n    <div class="container">\r\n        <br>\r\n        <div class="row">\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class="col-md-6">\r\n                <div class="row">\r\n                    <div class="col-md-1">\r\n                        <img src="http://pollev-com.umbler.net/wp-content/themes/storefront/assets/images/arrow-right.png" alt="">\r\n                    </div>\r\n                    <div class="col-md-11">\r\n                        <h5>Shopping Barra</h5>\r\n                        <h6>Avenida Centenário, 10 - Salvador - BA</h6>\r\n                        <h6>CEP 48005-105</h6>\r\n                        <h6>Tel.: (71) 0000-0000</h6>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>', 'Nossas Lojas | <span style="color: #e8e223; text-transform: none; letter-spacing: 0px;">Encontre a Pollev mais perto de você.</span>', '', 'inherit', 'closed', 'closed', '', '100-revision-v1', '', '', '2018-08-29 23:29:39', '2018-08-30 02:29:39', '', 100, 'http://pollev-com.umbler.net/2018/08/29/100-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_termmeta`
--

CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 15, 'product_count_product_cat', '0'),
(2, 16, 'order', '0'),
(3, 16, 'product_count_product_cat', '2'),
(4, 18, 'order', '0'),
(5, 18, 'display_type', ''),
(6, 18, 'thumbnail_id', '0'),
(7, 19, 'order', '0'),
(8, 19, 'display_type', ''),
(9, 19, 'thumbnail_id', '34'),
(10, 20, 'order', '0'),
(11, 20, 'display_type', ''),
(12, 20, 'thumbnail_id', '36'),
(13, 21, 'order', '0'),
(14, 21, 'display_type', ''),
(15, 21, 'thumbnail_id', '0'),
(16, 16, 'display_type', ''),
(17, 16, 'thumbnail_id', '32'),
(18, 20, 'product_count_product_cat', '1'),
(19, 19, 'product_count_product_cat', '3'),
(20, 22, 'order', '0'),
(21, 22, 'display_type', ''),
(22, 22, 'thumbnail_id', '0'),
(23, 23, 'order', '0'),
(24, 23, 'display_type', ''),
(25, 23, 'thumbnail_id', '0'),
(26, 24, 'order_pa_material', '0'),
(28, 27, 'order_pa_material', '0'),
(29, 18, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Sem categoria', 'sem-categoria', 0),
(16, 'Bags', 'bags', 0),
(17, 'Menu Principal', 'menu-principal', 0),
(18, 'Potes', 'potes', 0),
(19, 'Garrafas', 'garrafas', 0),
(20, 'Acessórios', 'acessorios', 0),
(21, 'Baby', 'baby', 0),
(22, 'Herméticos', 'hemerticos', 0),
(23, 'Com Divisórias', 'com-divisorias', 0),
(24, 'Vidro', 'vidro', 0),
(27, 'Plástico', 'plastico', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(40, 20, 0),
(9, 2, 0),
(9, 16, 0),
(40, 2, 0),
(49, 16, 0),
(47, 2, 0),
(47, 19, 0),
(49, 2, 0),
(55, 17, 0),
(56, 17, 0),
(57, 17, 0),
(58, 17, 0),
(59, 17, 0),
(62, 17, 0),
(63, 17, 0),
(73, 17, 0),
(47, 27, 0),
(74, 17, 0),
(75, 17, 0),
(76, 17, 0),
(77, 17, 0),
(78, 17, 0),
(79, 17, 0),
(80, 17, 0),
(85, 19, 0),
(85, 24, 0),
(85, 2, 0),
(117, 27, 0),
(117, 2, 0),
(117, 18, 0),
(127, 19, 0),
(127, 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 7),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(17, 17, 'nav_menu', '', 0, 15),
(16, 16, 'product_cat', '', 0, 2),
(18, 18, 'product_cat', '', 0, 1),
(19, 19, 'product_cat', '', 0, 3),
(20, 20, 'product_cat', '', 0, 1),
(21, 21, 'product_cat', '', 0, 0),
(22, 22, 'product_cat', '', 16, 0),
(23, 23, 'product_cat', '', 16, 0),
(24, 24, 'pa_material', 'vidro', 0, 1),
(27, 27, 'pa_material', 'Plástico', 0, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM AUTO_INCREMENT=339 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'pollev'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,theme_editor_notice'),
(15, 1, 'show_welcome_panel', '0'),
(234, 6, 'shipping_first_name', 'Adriano'),
(238, 6, 'shipping_city', 'Terra Nova'),
(237, 6, 'shipping_address_2', 'casa'),
(236, 6, 'shipping_address_1', 'Rua das Flores'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '114'),
(18, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:4:{s:32:"ec5decca5ed3d6b8079e2e7e7bacc9f2";a:11:{s:3:"key";s:32:"ec5decca5ed3d6b8079e2e7e7bacc9f2";s:10:"product_id";i:127;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:59;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:59;s:8:"line_tax";i:0;}s:32:"67c6a1e7ce56d3d6fa748ab6d9af3fd7";a:11:{s:3:"key";s:32:"67c6a1e7ce56d3d6fa748ab6d9af3fd7";s:10:"product_id";i:47;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:55;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:55;s:8:"line_tax";i:0;}s:32:"f457c545a9ded88f18ecee47145a72c0";a:11:{s:3:"key";s:32:"f457c545a9ded88f18ecee47145a72c0";s:10:"product_id";i:49;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:35;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:35;s:8:"line_tax";i:0;}s:32:"3ef815416f775098fe977004015c6193";a:11:{s:3:"key";s:32:"3ef815416f775098fe977004015c6193";s:10:"product_id";i:85;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:49.89999999999999857891452847979962825775146484375;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:49.89999999999999857891452847979962825775146484375;s:8:"line_tax";i:0;}}}'),
(37, 2, 'nickname', 'jussanajambeiro'),
(38, 2, 'first_name', 'Jussana'),
(39, 2, 'last_name', 'Jambeiro'),
(40, 2, 'description', ''),
(41, 2, 'rich_editing', 'true'),
(42, 2, 'syntax_highlighting', 'true'),
(43, 2, 'comment_shortcuts', 'false'),
(19, 1, 'wc_last_active', '1543017600'),
(20, 1, 'wp_user-settings', 'libraryContent=browse&editor=html&post_dfw=off'),
(21, 1, 'wp_user-settings-time', '1543069839'),
(28, 1, 'show_try_gutenberg_panel', '0'),
(29, 1, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:75:"dashboard_activity,woocommerce_dashboard_recent_reviews,dashboard_right_now";s:4:"side";s:68:"woocommerce_dashboard_status,dashboard_quick_press,dashboard_primary";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}'),
(30, 1, 'nav_menu_recently_edited', '17'),
(24, 1, 'envo-multipurpose_notice_ignore', 'true'),
(25, 1, 'dismissed_no_secure_connection_notice', '1'),
(26, 1, 'dismissed_store_notice_setting_moved_notice', '1'),
(27, 1, 'dismissed_jetpack_install_error_notice', '1'),
(31, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(32, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:18:"add-post-type-page";i:1;s:21:"add-post-type-product";i:2;s:12:"add-post_tag";i:3;s:15:"add-product_tag";}'),
(34, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(235, 6, 'shipping_last_name', 'da Silva de Jesus'),
(270, 7, 'session_tokens', 'a:1:{s:64:"175efb63f4582f17424f8e7f3f1fb211e7183a11c55fa64e1c87745c81395b10";a:4:{s:10:"expiration";i:1537269716;s:2:"ip";s:13:"177.52.100.21";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";s:5:"login";i:1536060116;}}'),
(269, 7, 'wp_user_level', '0'),
(36, 1, 'community-events-location', 'a:1:{s:2:"ip";s:12:"187.180.61.0";}'),
(44, 2, 'admin_color', 'fresh'),
(45, 2, 'use_ssl', '0'),
(46, 2, 'show_admin_bar_front', 'true'),
(47, 2, 'locale', ''),
(48, 2, 'wp_capabilities', 'a:1:{s:8:"customer";b:1;}'),
(49, 2, 'wp_user_level', '0'),
(50, 2, 'session_tokens', 'a:2:{s:64:"2bc7f1a6f090b26ac1b039e0169947b9bdce3b89f009cc1d9c1c219d4f50b924";a:4:{s:10:"expiration";i:1536103538;s:2:"ip";s:15:"177.134.179.117";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";s:5:"login";i:1534893938;}s:64:"8875eb5761dbbaccf6bfe8cc655a3820a4aa3d066e9740c87865c5704fb053cd";a:4:{s:10:"expiration";i:1535117238;s:2:"ip";s:14:"177.42.236.172";s:2:"ua";s:109:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36";s:5:"login";i:1534944438;}}'),
(51, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:3:{s:32:"67c6a1e7ce56d3d6fa748ab6d9af3fd7";a:11:{s:3:"key";s:32:"67c6a1e7ce56d3d6fa748ab6d9af3fd7";s:10:"product_id";i:47;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:55;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:55;s:8:"line_tax";i:0;}s:32:"f457c545a9ded88f18ecee47145a72c0";a:11:{s:3:"key";s:32:"f457c545a9ded88f18ecee47145a72c0";s:10:"product_id";i:49;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:35;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:35;s:8:"line_tax";i:0;}s:32:"d645920e395fedad7bbbed0eca3fe2e0";a:11:{s:3:"key";s:32:"d645920e395fedad7bbbed0eca3fe2e0";s:10:"product_id";i:40;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:80;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:80;s:8:"line_tax";i:0;}}}'),
(52, 2, 'last_update', '1534893938'),
(53, 2, 'billing_first_name', 'Jussana'),
(54, 2, 'billing_last_name', 'Jambeiro'),
(55, 2, 'billing_address_1', 'Dr Americo Silva 96'),
(56, 2, 'billing_city', 'salvador'),
(57, 2, 'billing_state', 'BA'),
(58, 2, 'billing_postcode', '40140-490'),
(59, 2, 'billing_country', 'BR'),
(60, 2, 'billing_email', 'jussanajambeiro@gmail.com'),
(61, 2, 'billing_phone', '71 999846930'),
(62, 2, 'shipping_first_name', 'Jussana'),
(63, 2, 'shipping_last_name', 'Jambeiro'),
(64, 2, 'shipping_address_1', 'Dr Americo Silva 96'),
(65, 2, 'shipping_city', 'salvador'),
(66, 2, 'shipping_state', 'BA'),
(67, 2, 'shipping_postcode', '40140-490'),
(68, 2, 'shipping_country', 'BR'),
(69, 2, 'shipping_method', 'a:1:{i:0;s:11:"flat_rate:1";}'),
(70, 2, 'wc_last_active', '1534896000'),
(268, 7, 'wp_capabilities', 'a:1:{s:8:"customer";b:1;}'),
(267, 7, 'locale', ''),
(266, 7, 'show_admin_bar_front', 'true'),
(265, 7, 'use_ssl', '0'),
(264, 7, 'admin_color', 'fresh'),
(263, 7, 'comment_shortcuts', 'false'),
(262, 7, 'syntax_highlighting', 'true'),
(261, 7, 'rich_editing', 'true'),
(260, 7, 'description', ''),
(323, 6, 'shipping_neighborhood', 'Centro'),
(313, 1, 'last_update', '1539042727'),
(322, 6, 'shipping_number', '03'),
(321, 6, 'billing_cellphone', ''),
(320, 6, 'billing_neighborhood', 'Centro'),
(319, 6, 'billing_number', '03'),
(318, 6, 'billing_sex', 'Masculino'),
(317, 6, 'billing_birthdate', '29/03/1995'),
(316, 6, 'billing_cpf', '063.075.645-71'),
(315, 6, 'shipping_method', 'a:1:{i:0;s:13:"correios-pac3";}'),
(241, 6, 'shipping_country', 'BR'),
(240, 6, 'shipping_postcode', '44270-000'),
(239, 6, 'shipping_state', 'BA'),
(233, 6, 'billing_phone', '-71- 9923-1995'),
(232, 6, 'billing_email', 'adriano.jesus@continuumweb.com.br'),
(231, 6, 'billing_country', 'BR'),
(314, 6, 'session_tokens', 'a:1:{s:64:"038da08b7ca80139245479a24290ee98ad71963926c9b8d213a0764dc29ce763";a:4:{s:10:"expiration";i:1539922631;s:2:"ip";s:14:"177.43.137.236";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";s:5:"login";i:1539749831;}}'),
(257, 7, 'nickname', 'Rafael'),
(258, 7, 'first_name', ''),
(229, 6, 'billing_state', 'BA'),
(230, 6, 'billing_postcode', '44270-000'),
(259, 7, 'last_name', ''),
(157, 1, 'closedpostboxes_product', 'a:0:{}'),
(158, 1, 'metaboxhidden_product', 'a:2:{i:0;s:10:"postcustom";i:1;s:7:"slugdiv";}'),
(228, 6, 'billing_city', 'Terra Nova'),
(126, 1, 'closedpostboxes_dashboard', 'a:2:{i:0;s:18:"dashboard_activity";i:1;s:17:"dashboard_primary";}'),
(127, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(224, 6, 'billing_first_name', 'Adriano'),
(225, 6, 'billing_last_name', 'da Silva de Jesus'),
(226, 6, 'billing_address_1', 'Rua das Flores'),
(227, 6, 'billing_address_2', 'casa'),
(326, 1, 'session_tokens', 'a:3:{s:64:"423d7cad9b46a5d8ade3abcbc1a637d3be9d08e9c59a5d028ad2487f44ce98c9";a:4:{s:10:"expiration";i:1543235444;s:2:"ip";s:13:"187.180.61.62";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36";s:5:"login";i:1543062644;}s:64:"ae23c2dbd95caf88bed3029bc4b8122712d084ea7a5b87121b2d032b79ca5cee";a:4:{s:10:"expiration";i:1543241743;s:2:"ip";s:13:"187.180.61.62";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36";s:5:"login";i:1543068943;}s:64:"ae71b29c8ac42e1b9e32d60b2a3c32f5fe5bac5f181798c374959862c69cc3cc";a:4:{s:10:"expiration";i:1543241795;s:2:"ip";s:13:"187.180.61.62";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36";s:5:"login";i:1543068995;}}'),
(223, 6, 'last_update', '1539750029'),
(325, 6, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:1:{s:32:"f457c545a9ded88f18ecee47145a72c0";a:11:{s:3:"key";s:32:"f457c545a9ded88f18ecee47145a72c0";s:10:"product_id";i:49;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:35;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:35;s:8:"line_tax";i:0;}}}'),
(253, 6, 'paying_customer', '1'),
(222, 6, 'wc_last_active', '1539734400'),
(305, 1, 'shipping_address_1', ''),
(306, 1, 'shipping_number', ''),
(307, 1, 'shipping_address_2', ''),
(308, 1, 'shipping_neighborhood', ''),
(309, 1, 'shipping_city', ''),
(310, 1, 'shipping_postcode', ''),
(311, 1, 'shipping_country', ''),
(312, 1, 'shipping_state', ''),
(219, 6, 'wp_user_level', '0'),
(218, 6, 'wp_capabilities', 'a:1:{s:8:"customer";b:1;}'),
(217, 6, 'locale', ''),
(216, 6, 'show_admin_bar_front', 'true'),
(207, 6, 'nickname', 'Adriano da Silva de Jesus'),
(208, 6, 'first_name', 'Adriano'),
(209, 6, 'last_name', 'da Silva de Jesus'),
(210, 6, 'description', ''),
(211, 6, 'rich_editing', 'true'),
(212, 6, 'syntax_highlighting', 'true'),
(213, 6, 'comment_shortcuts', 'false'),
(214, 6, 'admin_color', 'fresh'),
(215, 6, 'use_ssl', '0'),
(159, 5, 'nickname', 'Atilano Muinhos'),
(160, 5, 'first_name', 'Atilano'),
(161, 5, 'last_name', 'Muinhos'),
(162, 5, 'description', ''),
(163, 5, 'rich_editing', 'true'),
(164, 5, 'syntax_highlighting', 'true'),
(165, 5, 'comment_shortcuts', 'false'),
(166, 5, 'admin_color', 'fresh'),
(167, 5, 'use_ssl', '0'),
(168, 5, 'show_admin_bar_front', 'true'),
(169, 5, 'locale', ''),
(170, 5, 'wp_capabilities', 'a:1:{s:8:"customer";b:1;}'),
(171, 5, 'wp_user_level', '0'),
(271, 7, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:1:{s:32:"f457c545a9ded88f18ecee47145a72c0";a:11:{s:3:"key";s:32:"f457c545a9ded88f18ecee47145a72c0";s:10:"product_id";i:49;s:12:"variation_id";i:0;s:9:"variation";a:0:{}s:8:"quantity";i:1;s:9:"data_hash";s:32:"b5c1d5ca8bae6d4896cf1807cdf763f0";s:13:"line_tax_data";a:2:{s:8:"subtotal";a:0:{}s:5:"total";a:0:{}}s:13:"line_subtotal";d:35;s:17:"line_subtotal_tax";i:0;s:10:"line_total";d:35;s:8:"line_tax";i:0;}}}'),
(204, 5, '_woocommerce_persistent_cart_1', 'a:1:{s:4:"cart";a:0:{}}'),
(205, 1, 'meta-box-order_product', 'a:3:{s:4:"side";s:84:"postimagediv,woocommerce-product-images,product_catdiv,submitdiv,tagsdiv-product_tag";s:6:"normal";s:55:"postexcerpt,woocommerce-product-data,postcustom,slugdiv";s:8:"advanced";s:0:"";}'),
(206, 1, 'screen_layout_product', '2'),
(174, 5, 'wc_last_active', '1535155200'),
(175, 5, 'last_update', '1535058312'),
(176, 5, 'billing_first_name', 'Atilano'),
(177, 5, 'billing_last_name', 'Muinhos'),
(178, 5, 'billing_address_1', 'Rua Alceu Amoroso Lima'),
(179, 5, 'billing_address_2', 'Sala 618'),
(180, 5, 'billing_city', 'Salvador'),
(181, 5, 'billing_state', 'BA'),
(182, 5, 'billing_postcode', '41820-770'),
(183, 5, 'billing_country', 'BR'),
(184, 5, 'billing_email', 'atilano@yellowmelon.com.br'),
(185, 5, 'billing_phone', '-71- 99984-9797'),
(186, 5, 'shipping_first_name', 'Atilano'),
(187, 5, 'shipping_last_name', 'Muinhos'),
(188, 5, 'shipping_address_1', 'Rua Alceu Amoroso Lima'),
(189, 5, 'shipping_address_2', 'Sala 618'),
(190, 5, 'shipping_city', 'Salvador'),
(191, 5, 'shipping_state', 'BA'),
(192, 5, 'shipping_postcode', '41820-770'),
(193, 5, 'shipping_country', 'BR'),
(194, 5, 'shipping_method', 'a:1:{i:0;s:13:"correios-pac3";}'),
(195, 5, 'billing_cpf', '016.238.285-54'),
(196, 5, 'billing_birthdate', '03/07/1984'),
(197, 5, 'billing_sex', 'Masculino'),
(198, 5, 'billing_number', '786'),
(199, 5, 'billing_neighborhood', 'Caminho das Arvores'),
(200, 5, 'billing_cellphone', ''),
(201, 5, 'shipping_number', '786'),
(202, 5, 'shipping_neighborhood', 'Caminho das Arvores'),
(272, 7, 'wc_last_active', '1536019200'),
(286, 1, 'billing_first_name', ''),
(287, 1, 'billing_last_name', ''),
(288, 1, 'billing_cpf', ''),
(289, 1, 'billing_birthdate', ''),
(290, 1, 'billing_sex', ''),
(291, 1, 'billing_address_1', ''),
(292, 1, 'billing_number', ''),
(293, 1, 'billing_address_2', ''),
(294, 1, 'billing_neighborhood', ''),
(295, 1, 'billing_city', ''),
(296, 1, 'billing_postcode', ''),
(297, 1, 'billing_country', ''),
(298, 1, 'billing_state', ''),
(299, 1, 'billing_phone', ''),
(300, 1, 'billing_cellphone', ''),
(301, 1, 'billing_email', 'asjesus1994@gmail.com'),
(302, 1, 'shipping_first_name', ''),
(303, 1, 'shipping_last_name', ''),
(304, 1, 'shipping_company', ''),
(327, 1, 'closedpostboxes_shop_coupon', 'a:0:{}'),
(328, 1, 'metaboxhidden_shop_coupon', 'a:0:{}'),
(329, 1, '_order_count', '0'),
(330, 1, '_money_spent', '0'),
(331, 7, '_order_count', '0'),
(332, 7, '_money_spent', '0'),
(333, 6, '_order_count', '3'),
(334, 6, '_money_spent', '74.83'),
(335, 2, '_order_count', '1'),
(336, 2, '_money_spent', '0'),
(337, 5, '_order_count', '1'),
(338, 5, '_money_spent', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'pollev', '$P$BBVgGpIemgnPpyuTSdKsjkB/lHV9MU.', 'pollev', 'asjesus1994@gmail.com', '', '2018-08-15 03:46:05', '', 0, 'pollev'),
(2, 'jussanajambeiro', '$P$B7U7k6b8.LCmWVK3i2qAxPGQ/NHcZ4.', 'jussanajambeiro', 'jussanajambeiro@gmail.com', '', '2018-08-21 23:25:35', '', 0, 'jussanajambeiro'),
(7, 'Rafael', '$P$BrbPUZWLeGyPrTPRI5bsr/sBrptaZl1', 'rafael', 'rafael.meira@capgemini.com.br', '', '2018-09-04 11:21:46', '', 0, 'Rafael'),
(6, 'Adriano da Silva de Jesus', '$P$BIIUAGn4U0juRYP3N6RHONHhxg6obo.', 'adriano-da-silva-de-jesus', 'adriano.jesus@continuumweb.com.br', '', '2018-08-25 14:56:37', '', 0, 'Adriano da Silva de Jesus'),
(5, 'Atilano Muinhos', '$P$BmSZBpuqk7s5Pb.etRZTf74zpNBfv.1', 'atilano-muinhos', 'atilano@yellowmelon.com.br', '', '2018-08-23 21:00:43', '', 0, 'Atilano Muinhos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_wc_download_log`
--

CREATE TABLE IF NOT EXISTS `wp_wc_download_log` (
  `download_log_id` bigint(20) unsigned NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_wc_webhooks`
--

CREATE TABLE IF NOT EXISTS `wp_wc_webhooks` (
  `webhook_id` bigint(20) unsigned NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_api_keys`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_api_keys` (
  `key_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_api_keys`
--

INSERT INTO `wp_woocommerce_api_keys` (`key_id`, `user_id`, `description`, `permissions`, `consumer_key`, `consumer_secret`, `nonces`, `truncated_key`, `last_access`) VALUES
(1, 1, 'Pollev', 'read_write', '42be21d7c066a7690bca84ffe0137478559273da8d32a7c8eb1a8e8e39f7bcca', 'cs_6035ae76f7c345d7a3260060d410b40317db1384', NULL, '321e4a4', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) unsigned NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_attribute_taxonomies`
--

INSERT INTO `wp_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'material', 'Material', 'select', 'menu_order', 0),
(2, 'peso', 'Peso', 'select', 'name', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_log`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_log` (
  `log_id` bigint(20) unsigned NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_order_itemmeta`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `order_item_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_product_id', '47'),
(2, 1, '_variation_id', '0'),
(3, 1, '_qty', '1'),
(4, 1, '_tax_class', ''),
(5, 1, '_line_subtotal', '55'),
(6, 1, '_line_subtotal_tax', '0'),
(7, 1, '_line_total', '55'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(10, 2, 'method_id', 'flat_rate'),
(11, 2, 'instance_id', '1'),
(12, 2, 'cost', '10.00'),
(13, 2, 'total_tax', '0'),
(14, 2, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(15, 2, 'Itens', 'Garrafa Térmica &times; 1'),
(16, 3, '_product_id', '49'),
(17, 3, '_variation_id', '0'),
(18, 3, '_qty', '1'),
(19, 3, '_tax_class', ''),
(20, 3, '_line_subtotal', '35'),
(21, 3, '_line_subtotal_tax', '0'),
(22, 3, '_line_total', '35'),
(23, 3, '_line_tax', '0'),
(24, 3, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(25, 4, 'method_id', 'flat_rate'),
(26, 4, 'instance_id', '1'),
(27, 4, 'cost', '10.00'),
(28, 4, 'total_tax', '0'),
(29, 4, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(30, 4, 'Itens', 'Bolsa Térmica Bege &times; 1'),
(31, 5, '_product_id', '47'),
(32, 5, '_variation_id', '0'),
(33, 5, '_qty', '1'),
(34, 5, '_tax_class', ''),
(35, 5, '_line_subtotal', '55'),
(36, 5, '_line_subtotal_tax', '0'),
(37, 5, '_line_total', '55'),
(38, 5, '_line_tax', '0'),
(39, 5, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(40, 6, 'method_id', 'correios-pac'),
(41, 6, 'instance_id', '3'),
(42, 6, 'cost', '20.63'),
(43, 6, 'total_tax', '0'),
(44, 6, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(45, 6, '_delivery_forecast', '6'),
(46, 7, '_product_id', '49'),
(47, 7, '_variation_id', '0'),
(48, 7, '_qty', '1'),
(49, 7, '_tax_class', ''),
(50, 7, '_line_subtotal', '35'),
(51, 7, '_line_subtotal_tax', '0'),
(52, 7, '_line_total', '35'),
(53, 7, '_line_tax', '0'),
(54, 7, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(55, 8, 'method_id', 'correios-pac'),
(56, 8, 'instance_id', '3'),
(57, 8, 'cost', '39.83'),
(58, 8, 'total_tax', '0'),
(59, 8, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(60, 8, '_delivery_forecast', '8'),
(61, 9, '_product_id', '85'),
(62, 9, '_variation_id', '0'),
(63, 9, '_qty', '1'),
(64, 9, '_tax_class', ''),
(65, 9, '_line_subtotal', '49.9'),
(66, 9, '_line_subtotal_tax', '0'),
(67, 9, '_line_total', '49.9'),
(68, 9, '_line_tax', '0'),
(69, 9, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(70, 10, 'method_id', 'correios-sedex'),
(71, 10, 'instance_id', '4'),
(72, 10, 'cost', '27.83'),
(73, 10, 'total_tax', '0'),
(74, 10, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(75, 11, '_product_id', '49'),
(76, 11, '_variation_id', '0'),
(77, 11, '_qty', '1'),
(78, 11, '_tax_class', ''),
(79, 11, '_line_subtotal', '35'),
(80, 11, '_line_subtotal_tax', '0'),
(81, 11, '_line_total', '35'),
(82, 11, '_line_tax', '0'),
(83, 11, '_line_tax_data', 'a:2:{s:5:"total";a:0:{}s:8:"subtotal";a:0:{}}'),
(84, 12, 'method_id', 'correios-pac'),
(85, 12, 'instance_id', '3'),
(86, 12, 'cost', '42.83'),
(87, 12, 'total_tax', '0'),
(88, 12, 'taxes', 'a:1:{s:5:"total";a:0:{}}'),
(89, 12, '_delivery_forecast', '8');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_order_items`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) unsigned NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, 'Garrafa Térmica', 'line_item', 67),
(2, 'Taxa fixa', 'shipping', 67),
(3, 'Bolsa Térmica Bege', 'line_item', 70),
(4, 'Taxa fixa', 'shipping', 70),
(5, 'Garrafa Térmica', 'line_item', 82),
(6, 'PAC', 'shipping', 82),
(7, 'Bolsa Térmica Bege', 'line_item', 84),
(8, 'PAC', 'shipping', 84),
(9, 'Garrafa Térmica de Alumínio', 'line_item', 109),
(10, 'SEDEX', 'shipping', 109),
(11, 'Bolsa Térmica Bege', 'line_item', 112),
(12, 'PAC', 'shipping', 112);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `payment_token_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_payment_tokens`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) unsigned NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_sessions`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_sessions` (
  `session_id` bigint(20) unsigned NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=353 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_shipping_zones`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) unsigned NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_shipping_zones`
--

INSERT INTO `wp_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(2, 'Brasil', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) unsigned NOT NULL,
  `zone_id` bigint(20) unsigned NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_shipping_zone_locations`
--

INSERT INTO `wp_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(2, 2, 'BR', 'country');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) unsigned NOT NULL,
  `instance_id` bigint(20) unsigned NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) unsigned NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_woocommerce_shipping_zone_methods`
--

INSERT INTO `wp_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(2, 3, 'correios-pac', 1, 1),
(0, 2, 'flat_rate', 1, 1),
(2, 4, 'correios-sedex', 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_tax_rates`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) unsigned NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) unsigned NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) unsigned NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) unsigned NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2447;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=899;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=339;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=353;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
