<?php

/**

 * The header for our theme.

 *

 * Displays all of the <head> section and everything up till <div id="content">

 *

 * @package storefront

 */



?><!doctype html>

<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">

<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel='stylesheet' id='parent-style-css' href='https://pollev.com.br/wp-content/themes/storefront/pollev-style.css' type='text/css' media='all' />

<link rel="stylesheet" type="tex" href="/wp-content/themes/storefront/pollev-style.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="shortcut icon" type="image/png" href="/images/arrow-right.png"/>

<link rel="shortcut icon" type="image/png" href="https://pollev.com.br/wp-content/themes/storefront/assets/images/arrow-right.png"/>



<?php wp_head(); ?>

</head>



<body <?php body_class(); ?>>



<?php do_action( 'storefront_before_site' ); ?>



<div id="page" class="hfeed site">

	<?php do_action( 'storefront_before_header' ); ?>

	

	<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">

	<div class="container-fluid d-none d-sm-block">

		<div class="container">

			<div class="row link-top">

				<div class="col-md-2 offset-3">

					<a href="https://pollev.com.br/nossas-lojas/">Nossas lojas</a>

				</div>

				<div class="col-md-2">

					<a href="https://pollev.com.br/revenda/">Seja um revendedor</a>

				</div>	

				<div class="col-md-3 offset-2 right">

					<a href="https://pollev.com.br/minha-conta/">Minha Conta</a>

					<a href="https://pollev.com.br/atendimento/">Atendimento</a>					

				</div>

			</div>

		</div>

	</div>

		<?php

		

		/**

		 * Functions hooked into storefront_header action

		 *

		 * @hooked storefront_header_container                 - 0

		 * @hooked storefront_skip_links                       - 5

		 * @hooked storefront_social_icons                     - 10

		 * @hooked storefront_site_branding                    - 20

		 * @hooked storefront_secondary_navigation             - 30

		 * @hooked storefront_product_search                   - 40

		 * @hooked storefront_header_container_close           - 41

		 * @hooked storefront_primary_navigation_wrapper       - 42

		 * @hooked storefront_primary_navigation               - 50

		 * @hooked storefront_header_cart                      - 60

		 * @hooked storefront_primary_navigation_wrapper_close - 68

		 */

		do_action( 'storefront_header' ); ?>



	</header><!-- #masthead -->



	<?php

	/**

	 * Functions hooked in to storefront_before_content

	 *

	 * @hooked storefront_header_widget_region - 10

	 * @hooked woocommerce_breadcrumb - 10

	 */

	do_action( 'storefront_before_content' ); ?>



	<div id="content" class="site-content" tabindex="-1">

		

		<div class="col-full">



		<?php

		do_action( 'storefront_content_top' );

