<?php

/**

 * The template for displaying the footer.

 *

 * Contains the closing of the #content div and all content after

 *

 * @package storefront

 */



?>



		</div><!-- .col-full -->

	</div><!-- #content -->



	<?php do_action( 'storefront_before_footer' ); ?>



	<div class="formas-pgto">

		<div class="col-full">

			<div class="col-md-12">

				<div class="row title">

					<h4>FORMAS DE PAGAMENTO</h4>

				</div>

				<div class="row">

					<img src="/wp-content/themes/storefront/assets/images/cartoes.png" alt="">

				</div>

			</div>

			<div class="col-md-12 aviso">

				<div class="row" align="center">

					<h5>Os preços da loja online podem variar em relação as lojas fisicas e venda diretas.</h5>

				</div>

			</div>

		</div>

	</div>



	<div class="news-latter">

		<div class="col-full">

			<div class="col-md-12">

				<div class="row line-news-latter">

					<div class="col-md-6">

						<!-- to do -->

						<h3>Receba novidades exlusivas. <span style="color: #fff;">Cadastre-se!</span></h3>

					</div>

					<div class="col-md-6">

						<form>

							<div class="row">

								<div class="form-group col-md-6">

									<input type="email" class="form-control input-sm" id="email" value="" placeholder="digite seu email">

								</div>

								<div class="form-group col-md-4">

									<button class="btn btn-button btn-success" type="button">Enviar</button>

								</div>

							</div>

						</form>

					</div>

				</div>				

			</div>

							

		</div>

	</div>

	<div class="row-links-footer">

		<div class="col-full">

			<div class="col-md-12">

				<div class="row">

					<div class="col-md-2">

						<h4>Institucional</h4>

						<a href="#https://pollev.com.br/institucional/#geral">Condições Gerais</a><br>

						<a href="#https://pollev.com.br/institucional/#pagamentos">Formas de Pagamento</a><br>

						<a href="https://pollev.com.br/institucional/#novidades">Informativa e novidades</a><br>

						<a href="https://pollev.com.br/institucional/#politica">Politica de Privacidade</a><br>

						<a href="https://pollev.com.br/institucional/#conta">Sua Conta</a><br>

						<a href="https://pollev.com.br/institucional/#trocas">Trocas e devolução</a>						

					</div>

					<div class="col-md-2">

						<h4>Ajuda</h4>	

						<a href="https://pollev.com.br/minha-conta/orders/">Meus Pedidos</a><br>

						<a href="https://pollev.com.br/minha-conta/">Meu Cadastro</a>

					</div>

					<div class="col-md-2">

						<h4>Revenda</h4>

						<a href="https://pollev.com.br/consultoras/">Encontre uma loja ou um revendedor</a><br>

						<a href="https://pollev.com.br/revenda/">Seja um revendedor</a>

					</div>

					<div class="col-md-2">

						<h4>Atendimento</h4>

						<a href="#">Perguntas frequentes</a><br>	

						<a href="https://pollev.com.br/atendimento/">Fale conosco</a><br>

						<a href="https://pollev.com.br/minha-conta/orders/">Meus pedidos</a>

					</div>

					<div class="col-md-2">

					</div>

					<div class="col-md-2">

						<h4>Redes Sociais</h4>

						<div class="row icons">

							<a href="#"><img src="/wp-content/themes/storefront/assets/images/twitter.svg" alt=""></a>

							<a href="#"><img src="/wp-content/themes/storefront/assets/images/insta.svg" alt=""></a>

							<a href="#"><img src="/wp-content/themes/storefront/assets/images/face.svg" alt=""></a>

							<a href="#"><img src="/wp-content/themes/storefront/assets/images/g-plus.svg" alt=""></a>

						</div>

						

					</div>

				</div>

			</div>

		</div>

	</div>



	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="col-full">

			<div class="col-md-12">

				<div class="row">

					<div class="col-md-3 links-footer-down">

						<a href="https://pollev.com.br/politica-de-privacidade/">Politida de Privacidade</a><br>

						<a href="#">Alerta sobre segurança</a><br>

						<a href="#">Código de defesa do consumidor</a>

					</div>

					<div class="col-md-3 links-footer-down">

						<a href="#">Carga Tributária</a><br>

						<a href="#">Termos de uso</a>

					</div>

					<div class="col-md-3 offset-3">

						<div class="logo-negativo"></div>

					</div>

				</div>

			</div>





			<?php

			/**

			 * Functions hooked in to storefront_footer action

			 *

			 * @hooked storefront_footer_widgets - 10

			 * @hooked storefront_credit         - 20

			 */

			//do_action( 'storefront_footer' ); ?>



		</div><!-- .col-full -->

	</footer><!-- #colophon -->



	<?php do_action( 'storefront_after_footer' ); ?>



</div><!-- #page -->



<?php wp_footer(); ?>



</body>

</html>

